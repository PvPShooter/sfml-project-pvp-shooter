#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "IO.hpp"
#include "constantes.hpp"
#include "obstacles.hpp"

using namespace sf;
using namespace std;

static string oldData; /* The var for the old data received, in case of duplication
                        so that the data is not interpreted as the actions are already done*/

int main()
{
    srand(time(NULL));

    string serverName = "GYBE_SERVER";
    // MESSAGE IN CONSOLE
    cout << "*------------------------------------------*" << endl;
    cout << "Starting server " << serverName << endl;
    cout << "*------------------------------------------*" << endl;

    IpAddress ip1, ip2;
    //  THE CONNECTION

    TcpListener initListen;
    initListen.listen(53000);
    Packet packetClient1;
    Packet packetClient2;

    short mtrxObstcl[NB_COLUMN][NB_ROW] = {0};
    createMap(mtrxObstcl);

    ostringstream tmp;

    for(size_t i = 0; i < NB_COLUMN; i++)
        for(size_t j = 0; j < NB_ROW; j++)
            tmp << mtrxObstcl[i][j];

    string matrxSend = tmp.str();

    TcpSocket initSock;
    if  ( initListen.accept(initSock) == Socket::Done )
        ip1 = initSock.getRemoteAddress();

    packetClient1 << serverName << "CLIENT1" << matrxSend;
    initSock.send(packetClient1);

    initSock.disconnect();

    cout << "client1: " << ip1 << endl;

    if  ( initListen.accept(initSock) == Socket::Done )
        ip2 = initSock.getRemoteAddress();

    packetClient2 << serverName << "CLIENT2" << matrxSend;
    initSock.send(packetClient2);

    initSock.disconnect();
    initListen.close();


    cout << "client1: " << ip1 << ", client2: " << ip2 << endl;
    ///////////////////////////////////////////////////////////////////////


    UdpSocket serverSocket; // The socket
    string dataReceive, dataSend, sendedDuplicate, oldData;
    unsigned short int localPort = 53000;
    unsigned short int distPort = 53001;

    serverSocket.bind(localPort);

    while(1)
    {
        dataSend = dataReceive;  // We wait to send what we got

        if ( dataReceive.substr(0, dataReceive.find(":")) == "CLIENT1" && dataSend != sendedDuplicate )  // If beginning is CLIENT1 -> CLIENT2
        {
            IOFunc(&serverSocket, ip2, localPort, distPort, &dataSend, &dataReceive, &oldData, ONLY_SEND);
            sendedDuplicate = dataSend;
        }

        if ( dataReceive.substr(0, dataReceive.find(":")) == "CLIENT2" && dataSend != sendedDuplicate )  // If beginning is CLIENT2 -> CLIENT1
        {
            IOFunc(&serverSocket, ip1, localPort, distPort, &dataSend, &dataReceive, &oldData, ONLY_SEND);
            sendedDuplicate = dataSend;
        }

        dataSend = "";

        IOFunc(&serverSocket, ip1, localPort, distPort, &dataSend, &dataReceive, &oldData, ONLY_RECEIVE);
        IOFunc(&serverSocket, ip2, localPort, distPort, &dataSend, &dataReceive, &oldData, ONLY_RECEIVE);

    }
    return 0;
}

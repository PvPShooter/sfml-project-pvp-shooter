#include "personnage.hpp"
#include "constantes.hpp"

class Ghost
{
public:
    Ghost();
    //change la position
    void changePosition(float nouvelleposX, float nouvelleposY);
    //fais apparaitre le zombie aléatoirement
    void spawn();
    void ghostMove(Personnage &personnage,RenderWindow& app);
    void followUser(Personnage &personnage);
    void displayGhost(RenderWindow& app);
    void ghostDie();
    void loseLife();
    void showLife(RenderWindow &gameWindow);
    Texture ghostTexture;
    Sprite ghostSprite;
    std::string lifeSoundLink;
    std::string deathSoundLink;
    SoundBuffer lifeSoundBuffer;
    SoundBuffer deathSoundBuffer;
    SoundBuffer ghostTakeDamageBuf;
    Sound lifeSound;
    Sound deathSound;
    Sound damageSound;


    float angle;
    int life;
    Vector2f pos;

    float speed;
};

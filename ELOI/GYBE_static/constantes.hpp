#ifndef DEF_CONST
#define DEF_CONST
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <math.h>
#include <vector>

#define POLICE "upheavtt.ttf"
#define TAILLE_TREE 80
#define NB_ROW 10
#define NB_COLUMN 19
#define NB_OBSTACLE 7 //number of obstacle by object
#define BORDER_X 40
#define BORDER_Y 50
#define SCR_LARG 1600
#define SCR_HAUT 900
#define BUT_MENU_LARG 600
#define BUT_MENU_HAUT 150
#define TAIL_PERSO 70
#define BORD_LARG 40
#define TPS_RELOAD 20
#define TAIL_PROJ 30
#define GHOST "Sprites/Ghost.png"
#define X_MAT_TEST_LEFT (short)(((pos.x - TAIL_PERSO /2) - BORDER_X)/80)
#define X_MAT_TEST_RIGHT (short)(((pos.x + TAIL_PERSO /2) - BORDER_X)/80)
#define Y_MAT_TEST_UP (short)(((pos.y - TAIL_PERSO /2) - BORDER_Y)/80)
#define Y_MAT_TEST_DOWN (short)(((pos.y + TAIL_PERSO /2) - BORDER_Y)/80)
#define CORN_UL matriceObst[X_MAT_TEST_LEFT][Y_MAT_TEST_UP]
#define CORN_UR matriceObst[X_MAT_TEST_RIGHT][Y_MAT_TEST_UP]
#define CORN_DL matriceObst[X_MAT_TEST_LEFT][Y_MAT_TEST_DOWN]
#define CORN_DR matriceObst[X_MAT_TEST_RIGHT][Y_MAT_TEST_DOWN]

using namespace sf;

#endif

#include "obstacles.hpp"
#include "constantes.hpp"
#include <time.h>
#include <vector>
using namespace sf;
Background::Background()
{
    if (!background.loadFromFile("Sprites/background.png"))
        printf("PB de chargement de l'image  !\n");
    backGround.setTexture(background);
}

void Background::displayBg(RenderWindow& app) //We load the texture of the background if it is not load and we draw it
{
    app.draw(backGround);
}

int alea(int min, int max) //return an random value between a min value and a max value
{ int res = rand()%(max-min+1) +min ;
return res ; }

int aleaRow() //choose randomly a row in wish we will create an object we could use the function alea but it make the code easier to read
{ int res = rand()%NB_COLUMN ;
return res ; }

int aleaColumn() //choose randomly a column in wish we will create an object we could use the function alea but it make the code easier to read
{ int res = rand()%NB_ROW ;
return res ; }

void createMap(short matrxObstcl[][NB_ROW])
/* This function attributes values into the table matrxObstcl with 2 dimentions. The value can be set to: 1(grey wall), 2(brown wall, 3(bush) and 4(dead tree)*/
{
    int i,side,nbFence,nbWall,x,y,x2,y2,x3,y3,X,Y,typeWall,typeBush;
    for (i=0;i<NB_OBSTACLE*2;i++) //We want to create NB_OBSTACLE gray wall (7) and NB_OBSTACLE brown wall (7)
    {
		typeWall = alea(1,2); //We draw randomly a number between 1 and 2 to determine if the object is a gray wall (1) or a brown wall (2)
        side = alea(1,2); //we draw a random number to know which side (on the side or above) we extend the wall
        nbFence = alea(1,3); //number of walls to follow on the same line (from 1 to 3)
        if (side==1) //if the wall are spawn in a row
        {
             do //we draw randomly a case into the table, if there are already an number (wish mens there are an object at this position) we redo the draw until there are no number in the case draw randomly
            {
                x=aleaRow();
                if (x+nbFence>NB_COLUMN) //allow to eradicate a bug with spawned wall outside the map
                    x=x-nbFence+1;
                y=aleaColumn();
            }while(matrxObstcl[x][y]==1 && matrxObstcl[x+1][y]==1 && matrxObstcl[x+2][y]==1 || matrxObstcl[x][y]==2 && matrxObstcl[x+1][y]==2 && matrxObstcl[x+2][y]==2 || matrxObstcl[x][y]==3 && matrxObstcl[x+1][y]==3 && matrxObstcl[x+2][y]==3 || matrxObstcl[x][y]==4 && matrxObstcl[x+1][y]==4 && matrxObstcl[x+2][y]==4);
            matrxObstcl[x][y]=typeWall; //We set the value 1 (for a gray wall) or 2 (for a brown wall) depending of the value draw above into the case draw randomly
            if (nbFence>1) //We set the value 1 or 2 into the column next to the case draw previously in the same row if we have to draw 2 or more walls
            {
                matrxObstcl[x+1][y]=typeWall;
            }
            if(nbFence == 3)//We set the value 1 or 2 into the column next to the case draw previously in the same row if we have to draw 3 walls
            {
                matrxObstcl[x+2][y]=typeWall;
            }
        }
        else //same but if the wall are spawn in a column
        {
             do //we draw randomly a case into the table, if there are already an number (wish mens there are an object at this position) we redo the draw until there are no number in the case draw randomly
            {
                x=aleaRow();
                y=aleaColumn();
                if (y+nbFence>NB_ROW) //allow to eradicate a bug with spawned wall outside the map
                    y=y-nbFence+1;
            }while(matrxObstcl[x][y]==1 && matrxObstcl[x][y+1]==1 && matrxObstcl[x][y+2]==1 || matrxObstcl[x][y]==2 && matrxObstcl[x][y+1]==2 && matrxObstcl[x][y+2]==2 || matrxObstcl[x][y]==3 && matrxObstcl[x][y+1]==3 && matrxObstcl[x][y+2]==3 || matrxObstcl[x][y]==4 && matrxObstcl[x][y+1]==4 && matrxObstcl[x][y+2]==4);
            matrxObstcl[x][y]=typeWall; //We set the value 1 (for a gray wall) or 2 (for a brown wall) depending of the value draw above into the case draw randomly
            if (nbFence>1) //We set the value 1 or 2 into the row next to the case draw previously in the same column if we have to draw 2 or more walls
            {
                matrxObstcl[x][y+1]=typeWall;
            }
            if(nbFence == 3)//We set the value 1 or 2 into the row next to the case draw previously in the same column if we have to draw 3 walls
            {
                matrxObstcl[x][y+2]=typeWall;
            }
        }
    }
    for (i=0;i<NB_OBSTACLE*2;i++) //We want to create NB_OBSTACLE bush (7) and NB_OBSTACLE dead tree
    {
        typeBush = alea(3,4); //We draw randomly a number between 3 and 4 to determine if the object is a bush (3) or a dead tree (4)
        do//we draw randomly a case into the table, if there are already an number (wish mens there are an object at this position) we redo the draw until there are no number in the case draw randomly
        {
            x=aleaRow();
            y=aleaColumn();
        }while(matrxObstcl[x][y]==1 || matrxObstcl[x][y]==2 || matrxObstcl[x][y]==3 || matrxObstcl[x][y]==4);
        matrxObstcl[x][y]=typeBush; //We set the value 3 (for a bush) or 4 (for a dead tree) which are draw randomly above into the case draw randomly
    }
}
void LoadMap(short matrxObstcl[][NB_ROW],RenderWindow& app,std::vector<Sprite> &boxWall,std::vector<Sprite> &boxFence,std::vector<Sprite> &boxBush,std::vector<Sprite> &boxTree)
/*This function draw object in terms of the value into the table with 2 dimensions matrxObstcl*/
{
    int i,j,x,y;
    float nbWall = 0,nbFence = 0,nbBush = 0,nbTree = 0;

    /*We set the sprites  */
	Sprite tree;
	Sprite wall;
	Sprite bush;
	Sprite fence;

    for(i=0; i<NB_COLUMN; i++)
    {
        for(j=0; j<NB_ROW; j++) //Allow to read each line an column of the table
        {
            if (matrxObstcl[i][j]!=0) //if the value is 0 we have nothing to load so we don't need to test what number is in the case to draw an object: it allow to optimize the Loading of the map
            {
                x=i*80+BORDER_X; //calculate the position x and y of the object (each case of the table is 80 pixels larges and their are a border of 40 in x and 50 in y)
                y=j*80+BORDER_Y;
				if (matrxObstcl[i][j] == 1) //We create a sprite corresponding to a gray wall each time the value of the case is 1 and set the location of this sprite thanks to to the previous calculation
                {
                    boxWall.push_back(wall);
					boxWall[nbWall].setPosition(x,y); //We set the pos of the sprite number "nbWall"
					nbWall++;
                }
                else if (matrxObstcl[i][j] == 2) //We create a sprite corresponding to a brown wall each time the value of the case is 2  and set the location of this sprite thanks to to the previous calculation
                {
                    boxFence.push_back(fence);
					boxFence[nbFence].setPosition(x,y); //We set the pos of the sprite number "nbFence"
					nbFence++;
                }
                else if (matrxObstcl[i][j] == 3) //We create a sprite corresponding to a bush each time the value of the case is 3  and set the location of this sprite thanks to to the previous calculation
                {
                    boxBush.push_back(bush);
					boxBush[nbBush].setPosition(x,y); //We set the pos of the sprite number "nbBush"
					nbBush++;
                }
                else if (matrxObstcl[i][j] == 4) //We create a sprite corresponding to a dead tree each time the value of the case is 4  and set the location of this sprite thanks to to the previous calculation
                {
                    boxTree.push_back(tree);
					boxTree[nbTree].setPosition(x,y); //We set the pos of the sprite number "nbTree"
					nbTree++;
                }
            }
        }
    }
}

void DisplayMap(RenderWindow& app,std::vector<Sprite> &boxWall,std::vector<Sprite> &boxFence,std::vector<Sprite> &boxBush,std::vector<Sprite> &boxTree,Texture wallTexture,Texture fenceTexture,Texture bushTexture,Texture treeTexture)
{
	/*This function display the sprite created in the function LoadMap*/
	for (size_t i =0;i<boxWall.size();i++)
    {
        app.draw(boxWall[i]);
        boxWall[i].setTexture(wallTexture);
    }
	for (size_t i =0;i<boxFence.size();i++)
    {
        app.draw(boxFence[i]);
        boxFence[i].setTexture(fenceTexture);
    }
	for (size_t i =0;i<boxBush.size();i++)
    {
        app.draw(boxBush[i]);
        boxBush[i].setTexture(bushTexture);
    }
	for (size_t i =0;i<boxTree.size();i++)
    {
        app.draw(boxTree[i]);
        boxTree[i].setTexture(treeTexture);
    }
}


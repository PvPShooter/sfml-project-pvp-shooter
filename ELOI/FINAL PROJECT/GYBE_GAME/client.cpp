#include "client.hpp"
#include "IO.hpp"

using namespace sf;
using namespace std;

static string oldData; /* The var for the old data received, in case of duplication
                        so that the data is not interpreted as the actions are already done*/

void initClient(ICParams ic)
{
    *ic.ip = "";  // For the ip address

    // THE BEGINNING CONNECTION
    ostringstream ipPattern;
    ostringstream tmp;

    tmp << IpAddress::getLocalAddress();

    ipPattern << tmp.str().erase(tmp.str().rfind(".")+1, 5);
    tmp.str("");
    tmp.clear();

    tmp << ipPattern.str();

    int i;

    for (i = 0; i < 254; i++)
    {
        TcpSocket initSock;
        if (initSock.connect(*ic.ip, 53000, milliseconds(50)) != Socket::Done)
        {
            ipPattern << i;
            *ic.ip = ipPattern.str();
            cout << *ic.ip << endl;
            ipPattern.str("");
            ipPattern.clear();
            ipPattern << tmp.str();
        }
        else
        {
            Packet packet;
            initSock.receive(packet);

            packet >> *ic.serverName >> *ic.clientName >> *ic.mtrxSend;
            initSock.disconnect();
            break;
        }
    }
}

void Client(ClientParams cp)
{
    UdpSocket clientSocket;  // The socket
    unsigned short localPort = 53001;  // Local port
    unsigned short distPort = 53000;  // Dist port
    clientSocket.bind(localPort);  // We bind on the socket

while((*cp.gameIsWin)){
    // SENDING
    if ( (*cp.dataSend) != (*cp.sendedDuplicate) && !(*cp.dataSend).empty() )
    {
        IOFunc(&clientSocket, *cp.ip, localPort, distPort, cp.dataSend, cp.dataReceive, &oldData, ONLY_SEND);
        *cp.sendedDuplicate = (*cp.dataSend);
    }

    // RECEIVING
    IOFunc(&clientSocket, *cp.ip, localPort, distPort, cp.dataSend, cp.dataReceive, &oldData, ONLY_RECEIVE);

    if (( *cp.dataReceive) != (*cp.receivedDuplicate) && !(*cp.dataReceive).empty() )
    {
        size_t foundComma = (*cp.dataReceive).find(":");  // Find the ":"

        (*cp.dataReceive).erase(0, foundComma+1);  // We delete the "CLIENTX:" str

        foundComma = (*cp.dataReceive).find(":");

        // PLAYER PART
        (*cp.dataReceive).erase(0, foundComma+1);  // We delete the "PLAYER:"

        size_t foundPosX = (*cp.dataReceive).find("x");

        stringstream posXCoord( (*cp.dataReceive).substr(0, foundPosX) );
        int posXReceive;
        posXCoord >> posXReceive;

        (*cp.dataReceive).erase(0, foundPosX+1);
        size_t foundPosY = (*cp.dataReceive).find("y");

        stringstream posYCoord( (*cp.dataReceive).substr(0, foundPosY) );
        int posYReceive;
        posYCoord >> posYReceive;

        (*cp.player).changePosition(posXReceive, posYReceive);

        (*cp.dataReceive).erase(0, foundPosY+1);
        size_t foundAngle = (*cp.dataReceive).find("r");

        stringstream angle( (*cp.dataReceive).substr(0, foundAngle) );
        float angleReceive;
        angle >> angleReceive;

        (*cp.player).persoSprite.setRotation(angleReceive);

        // PROJECTILE PART
        foundComma = (*cp.dataReceive).find(":");
        (*cp.dataReceive).erase(0, foundComma+1);

        foundComma = (*cp.dataReceive).find(":");
        (*cp.dataReceive).erase(0, foundComma+1);  // We delete the "PROJ:"

         foundComma = (*cp.dataReceive).find(":");

         (*cp.shooting) = (*cp.dataReceive).substr(0, foundComma);
         cout << (*cp.shooting) << endl;

        (*cp.dataReceive).erase(0, foundComma+1);


        foundPosX = (*cp.dataReceive).find("x");

        stringstream mouseXCoord( (*cp.dataReceive).substr(0, foundPosX) );
        mouseXCoord >> (*cp.mouseX);

        (*cp.dataReceive).erase(0, foundPosX+1);
        foundPosY = (*cp.dataReceive).find("y");

        stringstream mouseYCoord( (*cp.dataReceive).substr(0, foundPosY) );
        mouseYCoord >> (*cp.mouseY);



        *cp.receivedDuplicate = (*cp.dataReceive);  // Not to duplicate

    }
}
}

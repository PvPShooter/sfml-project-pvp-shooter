#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED
#include <SFML/Graphics.hpp>
#include <time.h>
#include <SFML/Audio.hpp>
#include <math.h>



using namespace sf;

class Personnage
{
    public :

    Personnage();

    // change la position du sprite
    void changePosition(float nouvelleposX, float nouvelleposY);
    //change la texture du personnage
    void changeTexture(char link[ ]);
    //change la rotation du personnage
    void SuisLaMouse(RenderWindow &app);
    //Suit le clavier
    void SuisLeClavier(RenderWindow &app, Event &evenement);
    //affiche le sprite
    void afficherPerso(RenderWindow& app);
    //declaration du sprite et de la texture du sprite
    Sprite persoSprite;
    Texture persoTexture;
    int nbDeVie;
    Vector2f pos;
    char lienTexture[ ];

    private:



};

#endif // PERSONNAGE_H_INCLUDED

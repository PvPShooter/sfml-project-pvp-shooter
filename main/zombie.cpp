#include "zombie.h"



Zombie::Zombie()
{
    // chargement des textures
    if (!zombieTexture.loadFromFile(lienImage))
        printf("PB de chargement de l'image  !\n");
    zombieSprite.setTexture(zombieTexture);
    // chargement des sons
    if (!soundVieBuffer.loadFromFile("vie.wav"))
        printf("PB de chargement du son  !\n");
    soundVie.setBuffer(soundVieBuffer);

    if (!soundMortBuffer.loadFromFile("meurt.mp3"))
        printf("PB de chargement du son  !\n");
    soundMort.setBuffer(soundMortBuffer);

    zombieSprite.setOrigin(35,35);
}


void Zombie::spawn()
{

    int i= rand()%2 + 1;
    int y= rand()%2 + 1;
    if (i==1)
    {
        pos.x=rand()%LARGEUR+1;
        if (y==1)
        {
            pos.y=LONGEUR;
        }
        else
        {
            pos.y=0;
        }

    }
    else
    {


        pos.y=rand()%LARGEUR+1;
        if (y==1)
        {
            pos.x=LONGEUR;
        }
        else
        {
            pos.x=0;
        }
    }
    changePosition(pos.x,pos.y);
    soundVie.play();
}

void Zombie::afficheZombie(RenderWindow& app)
{
    app.draw(zombieSprite);
}

void Zombie::changePosition(float nouvelleposX, float nouvelleposY)
{
    pos.x= nouvelleposX;
    pos.y= nouvelleposY;
    zombieSprite.setPosition(pos.x,pos.y);
}

void Zombie::followPerso(Personnage& personnage)
{

}

/*void Zombie::touchePerso(Personnage &cible)
{
    if (Zombie.pos.x < )
}*/

void Zombie::zombieAvance(Personnage &cible, RenderWindow& app)
{
    Vector2f zombiePos =zombieSprite.getPosition();
    Vector2f persoPos =cible.persoSprite.getPosition();

    const float PI = 3.14159265;

    float dx = zombiePos.x - persoPos.x;
    float dy = zombiePos.y - persoPos.y;

    angle = (atan2(dy, dx)) * 180.000 /PI;
    //le zombie suit du regard le player
    zombieSprite.setRotation(angle+90);

    /* float avanceDeX = sin(angle *PI /180.000)*vitesse;
     float avanceDeY = cos(angle *PI /180.000)*vitesse;
    */
    if (zombiePos.x > persoPos.x)
        pos.x-=vitesse;
    else if (zombiePos.x == persoPos.x)
    {

    }
    else
        pos.x+=vitesse;

    if (zombiePos.y> persoPos.y)
        pos.y -=vitesse;
    else if (zombiePos.y == persoPos.y)
    {

    }
    else
        pos.y+=vitesse;

    changePosition(pos.x,pos.y);
    afficheZombie(app);

}

void Zombie::zombieMeurt()
{

}

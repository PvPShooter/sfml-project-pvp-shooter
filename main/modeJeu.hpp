#ifndef DEF_JEU
#define DEF_JEU
#include <SFML/Graphics.hpp>

using namespace sf;

void survivalGame(RenderWindow &gameWindow);
void multiGame(RenderWindow &gameWindow);

#endif // DEF_JEU

#include "modeJeu.hpp"
#include "constantes.hpp"
#include "personnage.hpp"
#include "projectile.hpp"


using namespace sf;

void survivalGame(RenderWindow &gameWindow)
{
    Event ev;
    Personnage player;
    Projectile shot;
    shot.setTexture("BalleJ1.png");
    int mtrxObstcl[20][20] = {0};
    player.changeTexture("Joueur1.png");
    player.changePosition(SCR_LARG/2, SCR_HAUT/2);

    //Zombie zombie;
    std::vector<Projectile> projectiles;
    projectiles.push_back(Projectile(shot));
    //std::vector<Zombie> zombies;
    //createMap();

    //Game Loop
    while (gameWindow.isOpen())
    {
        //Event Polling
        while (gameWindow.pollEvent(ev))
        {
            switch(ev.type)
            {
            case Event::Closed:
                gameWindow.close();
            }
        }

        //Update
        /*playerCenter = Vector2f(player.persoSprite.getPosition());//Vector2f(player.pos.x + TAIL_PERSO / 2, player.pos.y + TAIL_PERSO / 2);
        shot.setPosition(playerCenter);
        mousePosWindow = Vector2i(Mouse::getPosition(gameWindow));
        aimDir = mousePosWindow - playerCenter;
        aimDirNorm = aimDir / (float) sqrt(pow(aimDir.x, 2 )+ pow(aimDir.y , 2));*/
        shot.dirNormUpdate(player,ev,gameWindow);
        player.mvtPerso(ev);
        player.SuisLaMouse(gameWindow);
        if (player.reload < TPS_RELOAD)
        {
            player.reload++;
        }

        if (Mouse::isButtonPressed(Mouse::Left) && player.reload >= TPS_RELOAD)
        {
            player.reload = 0;
            shot.setPosition(Vector2f(player.persoSprite.getPosition()));
            shot.currVelocity = shot.aimDirNorm * shot.speed;
            projectiles.push_back(Projectile(shot));
        }

        for (size_t i = 0; i < projectiles.size(); i++)
        {
                if (mtrxObstcl[(int)(projectiles[i].pos.x + TAIL_PROJ / 2) / 80][(int)(projectiles[i].pos.y) / 80] == true || mtrxObstcl[(int)(projectiles[i].pos.x - TAIL_PROJ) / 80][(int)(projectiles[i].pos.y) / 80] == true || projectiles[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 <= BORD_LARG || projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 >= SCR_LARG - BORD_LARG) // IF HIT LEFT OR RIGHT
                {
                    projectiles[i].currVelocity.x = -projectiles[i].currVelocity.x;
                    projectiles[i].nbCol++;

                }
                if (mtrxObstcl[(int)(projectiles[i].pos.x) / 80][(int)(projectiles[i].pos.y  + TAIL_PROJ / 2) / 80] == true || mtrxObstcl[(int)(projectiles[i].pos.x) / 80][(int)(projectiles[i].pos.y - TAIL_PROJ / 2) / 80] == true || projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 <= BORD_LARG || projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 >= SCR_HAUT - BORD_LARG) // IF HIT UP OR DOWN
                {
                    projectiles[i].currVelocity.y = -projectiles[i].currVelocity.y;
                    projectiles[i].nbCol++;
                }
                if (projectiles[i].nbCol == 2)
                    projectiles.erase(projectiles.begin() + i);

             projectiles[i].projectileSprite.move(projectiles[i].currVelocity);
        }


        //Render



        gameWindow.clear(Color::Black);
        //loadMap(gameWindow, matrxObstcl[][]);


        /*for(size_t i = 0; i < zombies.size(); i++)
        {
            gameWindow.draw(zombies[i]);
        }*/
        for(size_t i = 0; i < projectiles.size(); i++)
        {
            projectiles[i].drawProj(gameWindow);
        }
        player.afficherPerso(gameWindow);

        gameWindow.display();
    }
}

void multiGame(RenderWindow &gameWindow)
{

}

#ifndef DEF_PERSO
#define DEF_PERSO
#include "constantes.hpp"





class Personnage
{
    public :

    Personnage();

    // change la position du sprite
    void changePosition(float nouvelleposX, float nouvelleposY);
    //change la texture du personnage
    void changeTexture(char link[ ]);
    //change la rotation du personnage
    void SuisLaMouse(RenderWindow &app);
    //Personnage bouge
    void mvtPerso (Event& evenement);
    //affiche le sprite
    void afficherPerso(RenderWindow& app);
    //declaration du sprite et de la texture du sprite
    Sprite persoSprite;
    Texture persoTexture;
    int nbDeVie;
    int reload;
    float angle;
    Vector2f pos;
    char lienTexture[ ];

    private:



};

#endif // DEF_PERSO

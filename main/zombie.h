#include "personnage.hpp"


class Zombie
{
public:
    Zombie();
    //change la position
    void changePosition(float nouvelleposX, float nouvelleposY);
    //fais apparaitre le zombie aléatoirement
    void spawn();
    void zombieAvance(Personnage &personnage,RenderWindow& app );
    void followPerso(Personnage &personnage);
    void afficheZombie(RenderWindow& app);
    void zombieMeurt();
    Texture zombieTexture;
    Sprite zombieSprite;
    std::string lienImage= "Zombie2.png";
    std::string lienSoundVie;
    std::string lienSoundMort;
    SoundBuffer soundVieBuffer;
    SoundBuffer soundMortBuffer;
    Sound soundVie;
    Sound soundMort;


    float angle;
    Vector2f pos;
private:
    float vitesse =5;
};

#include "personnage.hpp"
#include "constantes.hpp"

Personnage::Personnage()
{
    nbDeVie=3;
    pos.x=0;
    pos.y=0;
    persoSprite.setOrigin(TAIL_PERSO/2,TAIL_PERSO/2);
    reload = 0;
}

void Personnage::changePosition(float nouvelleposX, float nouvelleposY)
{
    pos.x= nouvelleposX;
    pos.y= nouvelleposY;
    persoSprite.setPosition(pos.x,pos.y);
}

void Personnage::changeTexture(char link[ ])
{

    if (!persoTexture.loadFromFile(link))
        printf("PB de chargement de l'image  !\n");
    persoSprite.setTexture(persoTexture);
}

void Personnage::afficherPerso(RenderWindow& app)
{
    app.draw(persoSprite);
}

void Personnage::SuisLaMouse(RenderWindow &app)
{
    Vector2f spritePos = persoSprite.getPosition();
    Vector2i positionSouris = Mouse::getPosition(app);

    const float PI = 3.14159265;

    float dx = spritePos.x - positionSouris.x;
    float dy = spritePos.y - positionSouris.y;

    angle = (atan2(dy, dx)) * 180.000 /PI;
    persoSprite.setRotation(angle);
}

void Personnage::mvtPerso (Event& evenement)
{
    if (Keyboard::isKeyPressed(Keyboard::Left) && pos.x - TAIL_PERSO / 2 > BORD_LARG)
        {
            pos.x-=1;
        }
        if (Keyboard::isKeyPressed(Keyboard::Right) && pos.x + TAIL_PERSO / 2 < SCR_LARG - BORD_LARG)
        {
            pos.x+=1;
        }
        if (Keyboard::isKeyPressed(Keyboard::Up) && pos.y - TAIL_PERSO / 2 > BORD_LARG)
        {
            pos.y-=1;
        }
        if (Keyboard::isKeyPressed(Keyboard::Down) && pos.y + TAIL_PERSO / 2 < SCR_HAUT - BORD_LARG)
        {
            pos.y+=1;
        }
        changePosition(pos.x,pos.y);
}



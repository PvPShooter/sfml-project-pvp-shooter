#include "obstacles.hpp"
#include "constantes.hpp"
#include <time.h>

using namespace sf;
int main()
{
    int positionX,positionY; //permet de tester
    std::vector<RectangleShape> box;
    srand(time(NULL));
    short matrxObstcl[NB_COLUMN][NB_ROW]={0}; //we create a table with 2 dimension in which we will save the pos of each obstacle
    RenderWindow app(VideoMode(SCR_LARG, SCR_HAUT), "MAP");
    app.clear();
/*We load the textures of the objects here so we don't load them several times*/
    Background bg;
    bg.displayBg(app); //we load the background, the value 1 say if we have to load the texture or not (1if we never load the texture and 0 if we already load the texture)
    createMap(matrxObstcl); //We define randomly the pos of the objects in the table matrxObstcl
    LoadMap(matrxObstcl,app); //treeTexture,wallTexture,fenceTexture,bushTexture,fence,wall,bush,tree, //we load the map with the table created above
    LoadCol(matrxObstcl,app,box);
/*allow me to test in a loop*/
    RectangleShape rectum(Vector2f(100, 100));
    rectum.setFillColor(Color::Yellow);
    app.display();
    Event evenement;
// tant que la fen�tre est ouverte
    while (app.isOpen()) //allow me to test in a loop
    {
// tant qu'il y a des �v�nements intercept�s par la fen�tre
        while (app.pollEvent(evenement))
        {
// je teste le type d��v�nement
            switch (evenement.type)
            {
// si fermeture de fen�tre d�clench�
            case Event::Closed:
                app.close();
                break;
//si touche appuy�e ( et non saisie utilisateur !)
            case Event::MouseMoved:
                positionY=evenement.mouseMove.y;
                positionX=evenement.mouseMove.x;
                break;
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Space)
                    {
                        positionX=800;
                        positionY=800;
                    }
                break;
            }
        rectum.setPosition(positionX,positionY);
        }
    app.clear();
    bg.displayBg(app); //we display the background
    LoadMap(matrxObstcl,app);//we display all the objects
    for (size_t i =0;i<box.size();i++)
    {
        app.draw(box[i]);
    }
    app.draw(rectum);
    app.display();
    }
    return 0;
}



#ifndef DEF_OBST
#define DEF_OBST
#include "obstacles.hpp"
#include "constantes.hpp"
#include <time.h>

using namespace sf;

class Tree //We create a class tree in which we can have a sprite for this tree
{
    public :
        Tree();
    void drawTree(int x,int y, RenderWindow& app); //allow us to draw the object, we can draw several time the same object at different pos if we don't erase the previous object
    Sprite tree;
    Texture treeTexture;
};

class Wall //We create a class wall in which we can have a sprite for this wall
{
    public :
        Wall();
    void newWall(int x,int y, RenderWindow& app);  //allow us to draw the object, we can draw several time the same object at different pos if we don't erase the previous object
    Sprite wall;
    Texture wallTexture;
};

class Fence //We create a class fence in which we can have a sprite for this fence
{
    public :
        Fence();
    void newFence(int x,int y, RenderWindow& app); //allow us to draw the object, we can draw several time the same object at different pos if we don't erase the previous object
    Sprite fence;
    Texture fenceTexture;
};

class Bush //We create a class bush in which we can have a sprite for this bush
{
    public :
        Bush();
    void newBush(int x,int y, RenderWindow& app); //allow us to draw the object, we can draw several time the same object at different pos if we don't erase the previous object
    Sprite bush;
    Texture bushTexture;
};

class Background //We create a class Background in which we will be able to load an background
{
    public :
        Background();
    void displayBg(RenderWindow& app);
    Sprite backGround;
    Texture background;
};

int alea (int min, int max) ; //return an random value between a min value and a max value
int aleaCollumn(); //choose randomly a row in wish we will create an object
int aleaRow(); //choose randomly a column in wish we will create an object
void createMap(short matrxObstcl[][NB_ROW]);
void LoadMap(short matrxObstcl[][NB_ROW],RenderWindow& app);
void LoadCol(short matrxObstcl[][NB_ROW],RenderWindow& app,std::vector<RectangleShape> &box);
#endif // DEF_OBST


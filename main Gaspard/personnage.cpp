#include "personnage.hpp"
#include "constantes.hpp"
#include <SFML/Audio.hpp>

using namespace sf;

Personnage::Personnage()
{
    nbDeVie=3;
    pos.x=0;
    pos.y=0;
    persoSprite.setOrigin(TAIL_PERSO/2,TAIL_PERSO/2);
    lifeTexture.loadFromFile("Sprites/Coeur.png");
    lifeSprite.setTexture(lifeTexture);
    reload = 0;
    score = 0;
    shootBuffer.loadFromFile("Audio/missile.wav");
    shoot.setBuffer(shootBuffer);
    loseLifeBuf.loadFromFile("Audio/damageJoueur.wav");
    loseLife.setBuffer(loseLifeBuf);
}

void Personnage::changePosition(float nouvelleposX, float nouvelleposY)
{
    pos.x= nouvelleposX;
    pos.y= nouvelleposY;
    persoSprite.setPosition(pos.x,pos.y);
}

void Personnage::changeTexture(char link[ ])
{

    if (!persoTexture.loadFromFile(link))
        printf("PB de chargement de l'image  !\n");
    persoSprite.setTexture(persoTexture);
}

void Personnage::afficherPerso(RenderWindow& app)
{
    app.draw(persoSprite);
}

void Personnage::SuisLaMouse(RenderWindow &app)
{
    Vector2f spritePos = persoSprite.getPosition();
    Vector2i positionSouris = Mouse::getPosition(app);

    const float PI = 3.14159265;

    float dx = spritePos.x - positionSouris.x;
    float dy = spritePos.y - positionSouris.y;

    angle = (atan2(dy, dx)) * 180.000 /PI;
    persoSprite.setRotation(angle);
}

void Personnage::mvtPerso (Event& evenement, short matriceObst[][NB_ROW])
{
        if (Keyboard::isKeyPressed(Keyboard::Left) && pos.x - TAIL_PERSO / 2 > BORDER_X && CORN_UL != 1 &&  CORN_DL != 1 && CORN_UL != 2 && CORN_DL != 2)
            pos.x-=5;
        if (Keyboard::isKeyPressed(Keyboard::Left) && !(pos.x - TAIL_PERSO / 2 > BORDER_X && CORN_UL != 1 &&  CORN_DL != 1 && CORN_UL != 2 && CORN_DL != 2))
            pos.x+=5;
        if (Keyboard::isKeyPressed(Keyboard::Right) && pos.x + TAIL_PERSO / 2 < SCR_LARG - BORDER_X && CORN_UR != 1 && CORN_DR != 1 && CORN_UR != 2 && CORN_DR != 2)
            pos.x+=5;
        if (Keyboard::isKeyPressed(Keyboard::Right) && !(pos.x + TAIL_PERSO / 2 < SCR_LARG - BORDER_X && CORN_UR != 1 && CORN_DR != 1 && CORN_UR != 2 && CORN_DR != 2))
            pos.x-=5;
        if (Keyboard::isKeyPressed(Keyboard::Up) && pos.y - TAIL_PERSO / 2 > BORDER_Y && CORN_UL != 1 && CORN_UR != 1 && CORN_UL != 2 && CORN_UR != 2)
            pos.y-=5;
        if (Keyboard::isKeyPressed(Keyboard::Up) && !(pos.y - TAIL_PERSO / 2 > BORDER_Y && CORN_UL != 1 && CORN_UR != 1 && CORN_UL != 2 && CORN_UR != 2))
            pos.y+=5;
        if (Keyboard::isKeyPressed(Keyboard::Down) && pos.y + TAIL_PERSO / 2 < SCR_HAUT - BORDER_Y && CORN_DL != 1 && CORN_DR != 1 && CORN_DL != 2 && CORN_DR != 2)
            pos.y+=5;
        if (Keyboard::isKeyPressed(Keyboard::Down) && !(pos.y + TAIL_PERSO / 2 < SCR_HAUT - BORDER_Y && CORN_DL != 1 && CORN_DR != 1 && CORN_DL != 2 && CORN_DR != 2))
            pos.y-=5;

        changePosition(pos.x,pos.y);
}

void Personnage::showLife(RenderWindow &gameWindow)
{
    for(size_t i = 0; i < nbDeVie*60; i+=60)
    {
        lifeSprite.setPosition(i+BORDER_X+10, BORDER_Y + 10);
        gameWindow.draw(lifeSprite);
    }
}

void Personnage::showScore(RenderWindow &gameWindow, Text text)
{
    char buffer[15];
    Text scoreText = text;
    sprintf(buffer, "SCORE : %i", score);
    scoreText.setString(buffer);
    scoreText.setCharacterSize(40);
    scoreText.setPosition(SCR_LARG - BORDER_X - 270, BORDER_Y + 5);
    gameWindow.draw(scoreText);
}



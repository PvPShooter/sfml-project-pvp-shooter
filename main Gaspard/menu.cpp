#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include "constantes.hpp"
#include "menu.hpp"
#include "server.hpp"

using namespace sf;

//TITLE SCREEN

int titleScreen(RenderWindow &gameWindow, Text &text)
{
     gameWindow.setMouseCursorVisible(true);
     Music menuMusic;
     menuMusic.openFromFile("Audio/mainMenu.ogg");
     menuMusic.play();
     menuMusic.setLoop(true);
     Text logoTXT = text, survivalText = text, logoFond, creditText = text, msgExit1 = text, msgExit2, msgExitOpt1, msgExitOpt2, multiplayerText; //Create all Texts
     int mode = 0;

     Texture textureTitre;
     if(!textureTitre.loadFromFile("Sprites/titre.png"))
        printf("error loading image titre.png");
     Sprite spriteTitre;
     spriteTitre.setTexture(textureTitre);

     // GAME TITLE
     spriteTitre.setPosition(SCR_LARG/2 - 1000 / 2, SCR_HAUT/8);
     // BUTTONS . SURVIVAL
     survivalText.setString("SURVIVAL");
     survivalText.setCharacterSize(100);
     survivalText.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 60 - 350, SCR_HAUT/3 + 200);
     survivalText.setColor(Color(42,36,36));
     RectangleShape survivalBox (Vector2f(BUT_MENU_LARG, BUT_MENU_HAUT)); // Clickable Box for survival
     survivalBox.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 - 350, SCR_HAUT/3 + 200);
     // BUTTONS . MULTIPLAYER
     multiplayerText = survivalText;
     multiplayerText.setString("MULTI");
     multiplayerText.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 450 , SCR_HAUT/3 + 200);
     RectangleShape multiplayerBox(Vector2f(BUT_MENU_LARG, BUT_MENU_HAUT)); // Clickable Box for survival
     multiplayerBox.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 300, SCR_HAUT/3 + 200);
     // BUTTONS . CREDITS
     creditText = survivalText;
     creditText.setString("CREDITS");
     creditText.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 90, SCR_HAUT/3 + 400);
     RectangleShape creditBox = survivalBox; //Clickable Box for credits
     creditBox.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2, SCR_HAUT/3 + 400);

     //IF LEAVE MSG BOX
     RectangleShape exitMsgFrame(Vector2f(800,400));
     exitMsgFrame.setFillColor(Color(65,46,23));
     exitMsgFrame.setPosition(SCR_LARG/2 - 400, SCR_HAUT / 2 - 200);
     RectangleShape exitMsgInbox(Vector2f(780,380));
     exitMsgInbox.setFillColor(Color(198,163,102));
     exitMsgInbox.setPosition(SCR_LARG/2 - 390, SCR_HAUT/2 - 190);
     msgExit1.setString("You have asked to exit the program,");
     msgExit1.setCharacterSize(35);
     msgExit1.setColor(Color(42,36,36));
     msgExit2 = msgExitOpt1 = msgExitOpt2 = msgExit1;
     msgExit2.setString("are you sure you want to leave?");
     msgExitOpt1.setString("Confirm");
     msgExitOpt2.setString("Cancel");
     msgExit1.setPosition(SCR_LARG / 2 - 370, SCR_HAUT / 2 - 170);
     msgExit2.setPosition(SCR_LARG / 2 - 370, SCR_HAUT / 2 - 120);
     msgExitOpt1.setPosition(SCR_LARG / 2 - 360, SCR_HAUT / 2 + 120);
     msgExitOpt2.setPosition(SCR_LARG / 2 + 160, SCR_HAUT / 2 + 120);
     RectangleShape opt1Ex(Vector2f(220, 55));
     RectangleShape opt2Ex = opt1Ex;
     opt1Ex.setPosition(SCR_LARG/2 - 370, SCR_HAUT / 2 + 115);
     opt2Ex.setPosition(SCR_LARG/2 + 150, SCR_HAUT / 2 + 115);





     int mouseX = 50, mouseY = 50; //Security (unknown values)
     bool menu = 1, stateExit = 0; //Gets value 0 if need to exit menu

     Event ev;

     while (menu)
     {
          //Event Polling
          while (gameWindow.pollEvent(ev))
          {
               switch (ev.type)
               {
               case Event::Closed:
                    gameWindow.close();
                    menu = false;
                    break;
               case Event::KeyPressed:
                    if (ev.key.code == Keyboard::Escape && stateExit == false) //If escape then ask if exit
                         stateExit = true;
                    else if (ev.key.code == Keyboard::Escape && stateExit == true) //If escape then go back
                        stateExit = false;
                    break;
               case Event::MouseMoved:
                    mouseX = ev.mouseMove.x;
                    mouseY = ev.mouseMove.y;
                    break;
               case Event::MouseButtonPressed:
                   if (stateExit == false)
                   {
                        if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 - 350 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 - 350 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
                         {
                             menu = false;
                             mode = 1;
                             menuMusic.stop();
                         }
                        if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 && mouseY > SCR_HAUT/3 + 400 && mouseY < SCR_HAUT/3 + 400 + BUT_MENU_HAUT)
                         creditScreen(gameWindow, text); //Credit Screen load if click on Credit Box

                        if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 300 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 300 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
                         {
                             mode = 2;
                             menu = multiMenu(gameWindow, text);
                             if (menu == true)
                                mode = 0;
                         }
                   }
                   else
                   {
                       if (mouseX > SCR_LARG/2 - 370 && mouseX < SCR_LARG/2 - 150 && mouseY > SCR_HAUT / 2 + 115 && mouseY < SCR_HAUT / 2 + 170)
                        gameWindow.close();
                       else if (mouseX > SCR_LARG/2 + 150 && mouseX < SCR_LARG/2 + 370 && mouseY > SCR_HAUT / 2 + 115 && mouseY < SCR_HAUT / 2 + 170)
                        stateExit = false;
                   }
               default:
                break;
               }
          }

          //Update

          if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 - 350 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 - 350 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
               survivalBox.setFillColor(Color(156,95,59));
          else
               survivalBox.setFillColor(Color(221,162,102));  //HOVER SURV BUTT CHANGE COLOR
          if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 300 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 300 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
               multiplayerBox.setFillColor(Color(156,95,59));
          else
               multiplayerBox.setFillColor(Color(221,162,102));  //HOVER SURV BUTT CHANGE COLOR
          if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 && mouseY > SCR_HAUT/3 + 400 && mouseY < SCR_HAUT/3 + 400 + BUT_MENU_HAUT)
               creditBox.setFillColor(Color(156,95,59));
          else
               creditBox.setFillColor(Color(221,162,102));    //HOVER CREDIT BUTT CHANGE COLOR
          if (mouseX > SCR_LARG/2 - 370 && mouseX < SCR_LARG/2 - 150 && mouseY > SCR_HAUT / 2 + 115 && mouseY < SCR_HAUT / 2 + 170)
               opt1Ex.setFillColor(Color(131,80,46));
          else
               opt1Ex.setFillColor(Color(150,97,61));
          if (mouseX > SCR_LARG/2 + 150 && mouseX < SCR_LARG/2 + 370 && mouseY > SCR_HAUT / 2 + 115 && mouseY < SCR_HAUT / 2 + 170)
               opt2Ex.setFillColor(Color(131,80,46));
          else
               opt2Ex.setFillColor(Color(150,97,61));

          //Render

            gameWindow.clear(Color(250,230,230));

          if (!stateExit)
          {
                gameWindow.draw(survivalBox);
                gameWindow.draw(survivalText);
                gameWindow.draw(creditBox);
                gameWindow.draw(creditText);
                gameWindow.draw(spriteTitre);
                gameWindow.draw(multiplayerBox);
                gameWindow.draw(multiplayerText);
          }
          else
          {
                gameWindow.draw(exitMsgFrame);
                gameWindow.draw(exitMsgInbox);
                gameWindow.draw(msgExit1);
                gameWindow.draw(msgExit2);
                gameWindow.draw(opt1Ex);
                gameWindow.draw(opt2Ex);
                gameWindow.draw(msgExitOpt1);
                gameWindow.draw(msgExitOpt2);
          }



          gameWindow.display();
     }
    return mode;
}

//CREDIT SCREEN

void creditScreen(RenderWindow &gameWindow, Text &text)
{

    Text returnText = text, creditTextp1 = text, creditTextp2, creditTextp3, creditTextp4, creditTextp5, creditTitle = text;
    //ALL CREDITS
    creditTitle.setString("CREDITS");
    creditTitle.setCharacterSize(150);
    creditTitle.setColor(Color(198,163,102));
    creditTitle.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 - 100);
    creditTextp1.setString("PROGRAMED BY :");
    creditTextp1.setColor(Color(42,36,36));
    creditTextp1.setCharacterSize(60);
    creditTextp2 = creditTextp3 = creditTextp4 = creditTextp5 = creditTextp1;
    creditTextp2.setString("DELFOSSE Gaspard");
    creditTextp3.setString("FOUBERT Eloi");
    creditTextp4.setString("GUIVARC'H Brieuc");
    creditTextp5.setString("CHAMOT Yannis");
    creditTextp1.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 + 50);
    creditTextp2.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 + 100);
    creditTextp3.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 + 150);
    creditTextp4.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 + 200);
    creditTextp5.setPosition(SCR_LARG * 0.1, SCR_HAUT / 5 + 250);

    // BUTTON . RETURN
    returnText.setCharacterSize(40);
    returnText.setColor(Color(42,36,36));
    returnText.setString("RETURN");
    returnText.setPosition(SCR_LARG * 0.1 + BUT_MENU_LARG/4 - 70, SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 + 5);
    RectangleShape returnBox (Vector2f(BUT_MENU_LARG / 2, BUT_MENU_HAUT / 2)); //Clickable Box to go back
    returnBox.setPosition(SCR_LARG * 0.1, SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2);


    int mouseX = 50, mouseY = 50; //Security (unknown values)
    bool credit = 1; //Gets value 0 if need to exit menu
    Event ev;

    //While no action happened
    while (credit)
    {
        //Event Polling
        while (gameWindow.pollEvent(ev))
        {
            switch (ev.type)
            {
            case Event::Closed:
                gameWindow.close();
                break;
            case Event::KeyPressed:
                if (ev.key.code == Keyboard::Escape)
                    credit = false;
                break;
            case Event::MouseMoved:
                mouseX = ev.mouseMove.x;
                mouseY = ev.mouseMove.y;
                break;
            case Event::MouseButtonPressed:
                    if (mouseX > SCR_LARG * 0.1 && mouseX < SCR_LARG * 0.1 + BUT_MENU_LARG /2 && mouseY > SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 && mouseY < SCR_HAUT * 0.9)
                         credit = false;
                    break;
            default:
                break;
            }
        }

        //Update

        if (mouseX > SCR_LARG * 0.1 && mouseX < SCR_LARG * 0.1 + BUT_MENU_LARG /2 && mouseY > SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 && mouseY < SCR_HAUT * 0.9)
               returnBox.setFillColor(Color(156,95,59));
        else
               returnBox.setFillColor(Color(221,162,102));  //HOVER RETURN BUTT CHANGE COLOR

        //Render

        gameWindow.clear(Color(250,230,230));

        gameWindow.draw(returnBox);
        gameWindow.draw(returnText);
        gameWindow.draw(creditTextp1);
        gameWindow.draw(creditTextp2);
        gameWindow.draw(creditTextp3);
        gameWindow.draw(creditTextp4);
        gameWindow.draw(creditTextp5);
        gameWindow.draw(creditTitle);


        gameWindow.display();

    }

}

//MULTIPLAYER MENU
bool multiMenu(RenderWindow &gameWindow, Text &text)
{

    Text hostGame = text, joinGame = text, returnText = text;
    bool menu = true;
    bool startGame = false;
    int mouseX = 50, mouseY = 50;
    Event ev;
     //BUTTON . HOST GAME
     hostGame.setString("HOST GAME");
     hostGame.setCharacterSize(100);
     hostGame.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30, SCR_HAUT/3 );
     hostGame.setColor(Color(42,36,36));
     RectangleShape hostBox (Vector2f(BUT_MENU_LARG, BUT_MENU_HAUT)); // Clickable Box for survival
     hostBox.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2, SCR_HAUT/3);
     // BUTTON . JOIN GAME
     joinGame = hostGame;
     joinGame.setString("JOIN GAME");
     joinGame.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30 , SCR_HAUT/3 + 200);
     RectangleShape joinBox(Vector2f(BUT_MENU_LARG, BUT_MENU_HAUT)); // Clickable Box for survival
     joinBox.setPosition(SCR_LARG / 2 - BUT_MENU_LARG / 2, SCR_HAUT/3 + 200);
     //BUTTON . RETURN
    returnText.setCharacterSize(40);
    returnText.setColor(Color(42,36,36));
    returnText.setString("RETURN");
    returnText.setPosition(SCR_LARG * 0.1 + BUT_MENU_LARG/4 - 70, SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 + 5);
    RectangleShape returnBox (Vector2f(BUT_MENU_LARG / 2, BUT_MENU_HAUT / 2)); //Clickable Box to go back
    returnBox.setPosition(SCR_LARG * 0.1, SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2);
    //NETWORK
    std::string dataSend;  // The var for the data sending
    std::string dataReceive;  // The var for the data we receive

    char who;  // For server or client
    bool quit = false;  // The boolean for quiting

    unsigned short localPort;  // For the local port of the server
    unsigned short distPort;  // For the distant port of the server

    Thread* serverThread = 0;  // The thread for the server

    //MENU
     while (menu == true)
     {
         //Event Polling
         while (gameWindow.pollEvent(ev))
         {
             switch (ev.type)
             {
             case Event::Closed:
                gameWindow.close();
                menu = false;
             case Event::KeyPressed:
                if (ev.key.code == Keyboard::Escape)
                    {
                        menu = false;
                        startGame = true;
                    }
                break;
             case Event::MouseMoved:
                mouseX = ev.mouseMove.x;
                mouseY = ev.mouseMove.y;
                break;
             case Event::MouseButtonPressed:
                if (mouseX > SCR_LARG * 0.1 && mouseX < SCR_LARG * 0.1 + BUT_MENU_LARG /2 && mouseY > SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 && mouseY < SCR_HAUT * 0.9)
                    {
                        menu = false;
                        startGame = true;
                    }
                if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 30 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
                {
                    menu = false;
					startGame = false;
                    //Start a server
                    std::string serverName;
                    std::cout << "Choose your server name: ";
                    std::cin >> serverName;

                    localPort = 53000;
                    distPort = 53001;

                    SParams ps = {localPort,distPort, &dataSend, &dataReceive, &quit, serverName}; // *dataSend, *dataReceive, *quit

                    serverThread = new Thread(&Server, ps);
                    serverThread->launch();  // Launch the server thread
                }
                if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 30 && mouseY > SCR_HAUT/3 && mouseY < SCR_HAUT/3 + BUT_MENU_HAUT)
                {
					menu = false;
					startGame = false;
                }
                break;
             default:
                break;
             }
         }
         //UPDATE
            if (mouseX > SCR_LARG * 0.1 && mouseX < SCR_LARG * 0.1 + BUT_MENU_LARG /2 && mouseY > SCR_HAUT * 0.9 - BUT_MENU_HAUT / 2 && mouseY < SCR_HAUT * 0.9)
               returnBox.setFillColor(Color(156,95,59));
            else
               returnBox.setFillColor(Color(221,162,102));  //HOVER RETURN BUTTON CHANGE COLOR
            if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 30 && mouseY > SCR_HAUT/3 + 200 && mouseY < SCR_HAUT/3 + 200 + BUT_MENU_HAUT)
               joinBox.setFillColor(Color(156,95,59));
            else
               joinBox.setFillColor(Color(221,162,102));  //HOVER SURV BUTT CHANGE COLOR
            if (mouseX > SCR_LARG / 2 - BUT_MENU_LARG / 2 + 30 && mouseX < SCR_LARG / 2 + BUT_MENU_LARG / 2 + 30 && mouseY > SCR_HAUT/3 && mouseY < SCR_HAUT/3 + BUT_MENU_HAUT)
               hostBox.setFillColor(Color(156,95,59));
            else
               hostBox.setFillColor(Color(221,162,102));  //HOVER SURV BUTT CHANGE COLOR


        //RENDER
        gameWindow.clear(Color(250,230,230));

        gameWindow.draw(hostBox);
        gameWindow.draw(joinBox);
        gameWindow.draw(joinGame);
        gameWindow.draw(hostGame);
        gameWindow.draw(returnBox);
        gameWindow.draw(returnText);

        gameWindow.display();
     }
    return startGame;
}


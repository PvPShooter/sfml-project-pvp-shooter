#include "obstacles.hpp"
#include "constantes.hpp"
#include <time.h>

using namespace sf;
int main()
{
    int positionX,positionY; //permet de tester
	short matrxObstcl[NB_COLUMN][NB_ROW]={0}; //we create a table with 2 dimension in which we will save the pos of each obstacle
	srand(time(NULL));

    std::vector<Sprite> boxWall;
    std::vector<Sprite> boxFence;
    std::vector<Sprite> boxBush;
    std::vector<Sprite> boxTree;

    Texture wallTexture;
	wallTexture.loadFromFile("wall.png");
    Texture fenceTexture;
    fenceTexture.loadFromFile("fence.png");
    Texture bushTexture;
	bushTexture.loadFromFile("bush.png");
    Texture treeTexture;
    treeTexture.loadFromFile("tree.png");

    RenderWindow app(VideoMode(SCR_LARG, SCR_HAUT), "MAP");
    app.clear();
/*We load the textures of the objects here so we don't load them several times*/
    Background bg;
    bg.displayBg(app); //we load the background, the value 1 say if we have to load the texture or not (1if we never load the texture and 0 if we already load the texture)
    createMap(matrxObstcl); //We define randomly the pos of the objects in the table matrxObstcl
    LoadMap(matrxObstcl,app,boxWall,boxFence,boxBush,boxTree); //we load the map with the table created above into sprites
/*allow me to test in a loop*/
    RectangleShape rectum(Vector2f(100, 100));
    rectum.setFillColor(Color::Yellow);
    app.display();
    Event evenement;
// tant que la fen�tre est ouverte
    while (app.isOpen()) //allow me to test in a loop
    {
// tant qu'il y a des �v�nements intercept�s par la fen�tre
        while (app.pollEvent(evenement)) //test
        {
// je teste le type d��v�nement
            switch (evenement.type)
            {
// si fermeture de fen�tre d�clench�
            case Event::Closed:
                app.close();
                break;
//si touche appuy�e ( et non saisie utilisateur !)
            case Event::MouseMoved:
                positionY=evenement.mouseMove.y; //test
                positionX=evenement.mouseMove.x; //test
                break;
            case Event::KeyPressed:
                if (evenement.key.code == Keyboard::Space)
                    {
                        positionX=800; //test
                        positionY=800; //test
                    }
                break;
            }
        rectum.setPosition(positionX,positionY); //test
        }
    app.clear();
    bg.displayBg(app); //we display the background
	DisplayMap(app,boxWall,boxFence,boxBush,boxTree,wallTexture,fenceTexture,bushTexture,treeTexture); //Display all the sprites load with the function LoadMap
    app.draw(rectum); //test
    app.display();
    }
    return 0;
}



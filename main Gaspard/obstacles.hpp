#ifndef DEF_OBST
#define DEF_OBST
#include "obstacles.hpp"
#include "constantes.hpp"
#include <time.h>

using namespace sf;

class Background //We create a class Background in which we will be able to load an background
{
    public :
        Background();
    void displayBg(RenderWindow& app);
    Sprite backGround;
    Texture background;
};

int alea (int min, int max) ; //return an random value between a min value and a max value
int aleaColumn(); //choose randomly a row in wish we will create an object
int aleaRow(); //choose randomly a column in wish we will create an object
void createMap(short matrxObstcl[][NB_ROW]);
void LoadMap(short matrxObstcl[][NB_ROW],RenderWindow& app,std::vector<Sprite> &boxWall,std::vector<Sprite> &boxFence,std::vector<Sprite> &boxBush,std::vector<Sprite> &boxTree);
void DisplayMap(RenderWindow& app,std::vector<Sprite> &boxWall,std::vector<Sprite> &boxFence,std::vector<Sprite> &boxBush,std::vector<Sprite> &boxTree,Texture wallTexture,Texture fenceTexture,Texture bushTexture,Texture treeTexture);
#endif // DEF_OBST


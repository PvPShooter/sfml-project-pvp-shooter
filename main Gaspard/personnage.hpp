#ifndef DEF_PERSO
#define DEF_PERSO
#include "constantes.hpp"
#include <SFML/Audio.hpp>


class Personnage
{
    public :

    Personnage();

    // change la position du sprite
    void changePosition(float nouvelleposX, float nouvelleposY);
    //change la texture du personnage
    void changeTexture(char link[ ]);
    //change la rotation du personnage
    void SuisLaMouse(RenderWindow &app);
    //Personnage bouge
    void mvtPerso (Event& evenement, short matrice[][NB_ROW]);
    //affiche le sprite
    void afficherPerso(RenderWindow& app);
    //declaration du sprite et de la texture du sprite
    void showLife(RenderWindow &gameWindow);
    void showScore(RenderWindow &gameWindow, Text text);
    Sprite persoSprite;
    Texture persoTexture;
    Sprite lifeSprite;
    Texture lifeTexture;
    sf::SoundBuffer shootBuffer;
    sf::SoundBuffer loseLifeBuf;
    sf::Sound loseLife;
    sf::Sound shoot;
    int nbDeVie;
    int score;
    int reload;
    float angle;
    Vector2f pos;
    char lienTexture[ ];

    private:



};

#endif // DEF_PERSO

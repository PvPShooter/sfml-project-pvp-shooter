#include "projectile.hpp"
#include "personnage.hpp"
#include "constantes.hpp"



Projectile::Projectile()
{
    nbCol = 0;
    projectileSprite.setOrigin(TAIL_PROJ/2, TAIL_PROJ/2);
    currVelocity.x = 0.f;
    currVelocity.y = 0.f;
    speed = 13;
}

void Projectile::projectileMvmt(int angle, Vector2f pos)
{
    pos.x += cos(angle);
    pos.y += sin(angle);
}

void Projectile::drawProj(RenderWindow &gameWindow)
{
    gameWindow.draw(projectileSprite);
}

void Projectile::setTexture(char link[])
{
    if (!projectileTexture.loadFromFile(link))
        printf("PB de chargement de l'image  !\n");
    projectileSprite.setTexture(projectileTexture);
}

void Projectile::setPosition(Vector2f NVpos)
{
    pos.x = NVpos.x;
    pos.y = NVpos.y;
    projectileSprite.setPosition(pos.x - TAIL_PROJ / 2, pos.y - TAIL_PROJ / 2);
}

void Projectile::dirNormUpdate(Personnage& player, Event& ev, RenderWindow& gameWindow)
{
        Vector2f playerCenter = Vector2f(player.persoSprite.getPosition());
        setPosition(playerCenter);
        Vector2f mousePosWindow = Vector2f(Mouse::getPosition(gameWindow));
        Vector2f aimDir = mousePosWindow - playerCenter;
        aimDirNorm = aimDir / (float) sqrt(pow(aimDir.x, 2 )+ pow(aimDir.y , 2));
}

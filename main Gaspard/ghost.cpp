#include <SFML/Audio.hpp>
#include "ghost.hpp"
#include "constantes.hpp"

using namespace sf;

Ghost::Ghost()
{
    // chargement des textures
    if (!ghostTexture.loadFromFile(GHOST))
        printf("PB de chargement de l'image  !\n");
    ghostSprite.setTexture(ghostTexture);
    // chargement des sons
    if (!lifeSoundBuffer.loadFromFile("Audio/spawnGhost.wav"))
        printf("PB de chargement du son  !\n");
    lifeSound.setBuffer(lifeSoundBuffer);

    if (!deathSoundBuffer.loadFromFile("Audio/deathGhost.wav"))
        printf("PB de chargement du son  !\n");
    deathSound.setBuffer(deathSoundBuffer);

    if (!ghostTakeDamageBuf.loadFromFile("Audio/damageGhost.wav"))
        printf("PB de chargement du son  !\n");
    damageSound.setBuffer(ghostTakeDamageBuf);

    ghostSprite.setOrigin(35,35);

    life = 50;
    speed = 0.8;
}


void Ghost::spawn()
{

    int i= rand()%2 + 1;
    int y= rand()%2 + 1;
    if (i==1)
    {
        pos.x=rand()%SCR_LARG+1;
        if (y==1)
        {
            pos.y=SCR_HAUT;
        }
        else
        {
            pos.y=1;
        }

    }
    else
    {


        pos.y=rand()%SCR_HAUT+1;
        if (y==1)
        {
            pos.x=SCR_LARG;
        }
        else
        {
            pos.x=1;
        }
    }
    changePosition(pos.x,pos.y);
    lifeSound.play();
}

void Ghost::displayGhost(RenderWindow& app)
{
    app.draw(ghostSprite);
}

void Ghost::changePosition(float nouvelleposX, float nouvelleposY)
{
    pos.x= nouvelleposX;
    pos.y= nouvelleposY;
    ghostSprite.setPosition(pos.x,pos.y);
}

void Ghost::followUser(Personnage& personnage)
{

}

/*void Zombie::touchePerso(Personnage &cible)
{
    if (Zombie.pos.x < )
}*/

void Ghost::ghostMove(Personnage &cible, RenderWindow& app)
{
    Vector2f ghostPos =ghostSprite.getPosition();
    Vector2f playerPos =cible.persoSprite.getPosition();

    const float PI = 3.14159265;

    float dx = ghostPos.x - playerPos.x;
    float dy = ghostPos.y - playerPos.y;

    angle = (atan2(dy, dx)) * 180.000 /PI;
    //le zombie suit du regard le player
    ghostSprite.setRotation(angle+90);

    /* float avanceDeX = sin(angle *PI /180.000)*vitesse;
     float avanceDeY = cos(angle *PI /180.000)*vitesse;
    */
    if (ghostPos.x > playerPos.x)
        pos.x-=speed;
    else if (ghostPos.x == playerPos.x)
    {

    }
    else
        pos.x+=speed;

    if (ghostPos.y> playerPos.y)
        pos.y -=speed;
    else if (ghostPos.y == playerPos.y)
    {

    }
    else
        pos.y+=speed;

}

void Ghost::ghostDie()
{
    deathSound.play();
}

void Ghost::showLife(sf::RenderWindow &gameWindow)
{
    sf::RectangleShape lifeBarRed(sf::Vector2f(50, 10));
    sf::RectangleShape lifeBar(sf::Vector2f(life, 10));
    lifeBarRed.setFillColor(Color::Red);
    lifeBar.setFillColor(Color::Green);
    lifeBarRed.setPosition(ghostSprite.getPosition().x - 25, ghostSprite.getPosition().y - TAIL_PERSO/2 - 40);
    lifeBar.setPosition(ghostSprite.getPosition().x - 25, ghostSprite.getPosition().y - TAIL_PERSO/2 - 40);
    gameWindow.draw(lifeBarRed);
    gameWindow.draw(lifeBar);
}

void Ghost::loseLife()
{
    life -= 10;
    damageSound.play();
}

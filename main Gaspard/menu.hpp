#ifndef DEF_MENU
#define DEF_MENU

using namespace sf;

int titleScreen(RenderWindow &gameWindow, Text &text);
void creditScreen(RenderWindow &gameWindow, Text &text);
bool multiMenu(RenderWindow &gameWindow, Text &text);

#endif // DEF_MENU

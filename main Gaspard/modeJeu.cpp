/*
            This Function is the survival games structure;
        It connects every little programs to make the final game work
*/


#include <SFML/Audio.hpp>
#include "modeJeu.hpp"
#include "constantes.hpp"
#include "personnage.hpp"
#include "projectile.hpp"
#include "ghost.hpp"
#include "obstacles.hpp"
#include "IO.hpp"
#include "client.hpp"
#include "server.hpp"


using namespace sf;
using namespace std;

void survivalGame(RenderWindow &gameWindow, Text textMod)
{
//                                      PRE GAME SETTINGS
/*-------------------------------------------------------------------------------------------*/

    //LOAD FILES

        std::vector<Sprite> boxWall;        /*                       */
        std::vector<Sprite> boxFence;       /*      MAP VECTORS      */
        std::vector<Sprite> boxBush;        /*                       */
        std::vector<Sprite> boxTree;        /*                       */

        //TEXTURES FOR MAP STUFF

            Texture wallTexture;
            wallTexture.loadFromFile("Sprites/wall.png");
            Texture fenceTexture;
            fenceTexture.loadFromFile("Sprites/fence.png");
            Texture bushTexture;
            bushTexture.loadFromFile("Sprites/bush.png");
            Texture treeTexture;
            treeTexture.loadFromFile("Sprites/tree.png");
            Texture aimTexture;
            Background bg;

        //AIM SPRITE SETUP

            aimTexture.loadFromFile("Sprites/aim.png");
            Sprite aim;
            aim.setTexture(aimTexture);
            aim.setOrigin(10,10);

        //SOUNDS LOAD

            Sound shoot;
            Music music;
            music.openFromFile("Audio/gameSound.ogg");  //Start music over and over
            music.play();
            music.setLoop(true);
            SoundBuffer gameOverBuf;                    //Play when the player dies
            gameOverBuf.loadFromFile("Audio/gameOver.wav");
            Sound gameOverSound;
            gameOverSound.setBuffer(gameOverBuf);

    //WINDOW SECURITY

        gameWindow.setFramerateLimit(60);
        gameWindow.setMouseCursorVisible(false);

    //CREATE A NEW MAP

        short mtrxObstcl[NB_COLUMN][NB_ROW] = {0};                          //Used for map display and detections of walls
        bg.displayBg(gameWindow);                                           //we load the background, the value 1 say if we have to load the texture or not (1if we never load the texture and 0 if we already load the texture)
        createMap(mtrxObstcl);                                              //We define randomly the pos of the objects in the table matrxObstcl
        LoadMap(mtrxObstcl,gameWindow,boxWall,boxFence,boxBush,boxTree);    //we load the map with the table created above into sprites

/*-------------------------------------------------------------------------------------------*/

   //VARIABLES

        Event ev;
        Personnage player;
        player.changeTexture("Sprites/Joueur1.png");
        player.changePosition(SCR_LARG/2, SCR_HAUT/2);
        Projectile shot;
        shot.setTexture("Sprites/BalleJ1.png");
        Ghost ghost;

        int ghostSpawn = 750, newGhost = 900, increaseGhostSpeed = 0, invinsibleTime = 50, gameIsWin = true;

        //VECTORS

            std::vector<Projectile> projectiles;
            std::vector<Ghost> ghosts;



   //GAME LOOP

        while (gameIsWin)
        {

        //EVENT POLLING

            while (gameWindow.pollEvent(ev))
            {
                switch(ev.type)
                {
                case Event::Closed:
                    gameWindow.close();
                    break;
                default:
                    break;
                }
            }

        //UPDATES

            //Update aim

                aim.setPosition(Mouse::getPosition(gameWindow).x, Mouse::getPosition(gameWindow).y);

            //Update aim direction

                shot.dirNormUpdate(player,ev,gameWindow);

            //Update player

                player.mvtPerso(ev, mtrxObstcl);
                player.SuisLaMouse(gameWindow);

                if (player.reload < TPS_RELOAD)  //Reload time
                {
                    player.reload++;
                }

            //Create a new projectile

                if (Mouse::isButtonPressed(Mouse::Left) && player.reload >= TPS_RELOAD) //Needs to have reloaded before shooting
                {
                    player.reload = 0;
                    shot.setPosition(Vector2f(player.persoSprite.getPosition()));
                    shot.currVelocity = shot.aimDirNorm * shot.speed;
                    projectiles.push_back(Projectile(shot));
                    player.shoot.play();
                }

            //Update projectile

                for (size_t i = 0; i < projectiles.size(); i++)
                {

                    //Collision detections

                    //Vertical Side Borders
                    if (projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 <= BORDER_X || projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 >= SCR_LARG - BORDER_X) // IF HIT LEFT OR RIGHT
                    {
                        projectiles[i].currVelocity.x = -projectiles[i].currVelocity.x;
                        projectiles[i].nbCol++;
                    }
                    //Vertical Side Blocks
                    if (mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectiles[i].currVelocity.x = -projectiles[i].currVelocity.x;
                        projectiles[i].nbCol++;
                        projectiles[i].projectileSprite.move(projectiles[i].currVelocity);
                    }
                    //Horizontal Side Border
                    if (projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 <= BORD_LARG || projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 >= SCR_HAUT - BORD_LARG) // IF HIT UP OR DOWN
                    {
                        projectiles[i].currVelocity.y = -projectiles[i].currVelocity.y;
                        projectiles[i].nbCol++;
                    }
                    //Horizontal Side Blocks
                    if (mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectiles[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectiles[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectiles[i].currVelocity.y = -projectiles[i].currVelocity.y;
                        projectiles[i].nbCol++;
                        projectiles[i].projectileSprite.move(projectiles[i].currVelocity);
                    }
                    //If projectile collided twice, then destroy object
                        if (projectiles[i].nbCol >= 3)
                            projectiles.erase(projectiles.begin() + i);
                    //Else test if projectile hit a ghost
                        else
                        {
                            for (size_t j = 0; j < ghosts.size(); j++)
                            {
                                if (projectiles[i].projectileSprite.getGlobalBounds().intersects(ghosts[j].ghostSprite.getGlobalBounds())) //If projectile hit a ghost
                                {
                                    ghosts[j].loseLife(); //Ghost lose life
                                        if (ghosts[j].life == 0) //If ghost life at 0
                                        {
                                            ghost.deathSound.play(); //Ghost dies
                                            ghosts.erase(ghosts.begin() + j);
                                            player.score += 100;
                                        }
                                    projectiles.erase(projectiles.begin() + i); //Destroy projectile that hit ghost
                                    break; //breaks so it doesn't test again with this projectile for another ghost
                                }
                            }
                        }
                    projectiles[i].projectileSprite.move(projectiles[i].currVelocity); //Update projectile Position
                }

            //Update Ghost

                //Every 1000 tics, ghost speed increase
                increaseGhostSpeed++;
                if (increaseGhostSpeed == 1000)
                {
                    ghost.speed += 0.1;
                    increaseGhostSpeed = 0;
                }

                if (invinsibleTime < 200)
                    invinsibleTime ++; //If player gets hit


                for (size_t i = 0; i < ghosts.size(); i++)
                {
                    //Update ghost position
                    ghosts[i].ghostMove(player, gameWindow);
                    ghosts[i].changePosition(ghosts[i].pos.x,ghosts[i].pos.y);

                    //If ghost hit player
                        if (ghosts[i].ghostSprite.getGlobalBounds().intersects(player.persoSprite.getGlobalBounds()) && invinsibleTime >= 150 )
                        {
                            player.loseLife.play(); //player lose life
                            player.nbDeVie -= 1;
                            invinsibleTime = 0; //Have invinsibility for 150 tics
                        }
                }

                //Check if new ghost have to spawn
                if (ghostSpawn < newGhost) //If not then timer to new increase
                {
                    ghostSpawn++;
                }
                else if (ghostSpawn == newGhost && ghosts.size() < 15) //If yes and less than 15 ghost on the map
                {
                    ghost.spawn(); //Spawn new ghost
                    ghosts.push_back(Ghost(ghost));
                    ghostSpawn = 0; //Timer till new ghost reset
                    if (newGhost >= 300)
                        newGhost -= 100; //set a faster spawn
                    else if (newGhost >= 130)
                    newGhost -= 10;
                }

        //RENDER

            gameWindow.clear(Color::Black); //Clear window

            bg.displayBg(gameWindow); //we display the background
            player.afficherPerso(gameWindow); //Display player under the sprites of the map so that the bushes and dead trees are on top of the player
            DisplayMap(gameWindow,boxWall,boxFence,boxBush,boxTree,wallTexture,fenceTexture,bushTexture,treeTexture); //Display all the sprites load with the function LoadMap
            //Draw all projectiles

                for(size_t i = 0; i < projectiles.size(); i++)
                {
                    projectiles[i].drawProj(gameWindow);
                }

            //Draw all ghosts

                for(size_t i = 0; i < ghosts.size(); i++)
                {
                    ghosts[i].displayGhost(gameWindow);
                    ghosts[i].showLife(gameWindow);
                }

            //Show score and life

                player.showScore(gameWindow, textMod);
                player.showLife(gameWindow);

            //Draw aim on top of everything

                gameWindow.draw(aim);

            //Display window

                gameWindow.display();

        //CHECK IF PLAYER LOST

            if (player.nbDeVie == 0)
                {
                    sleep(seconds(1)); //Break for 1 seconds
                    music.stop();
                    gameOverSound.play();
                    gameIsWin = gameOverScreen(gameWindow, textMod, player.score, gameIsWin);   //Game Over Screen
                }
        }

    //END OF APPLICATION

}

int gameOverScreen(RenderWindow &gameWindow, Text textMod, int score, int stopGame)
{
    Texture gameOverTex;
    Event ev;
    Text pressButtonForTitle = textMod;
    char buffer[15];
    gameOverTex.loadFromFile("Sprites/gameover.png");
    Sprite gameOver;
    gameOver.setTexture(gameOverTex);
    gameOver.setPosition(SCR_LARG / 2 - 600, SCR_HAUT / 3 - 200);
    sprintf(buffer, "SCORE : %i", score);
    textMod.setString(buffer);
    textMod.setCharacterSize(100);
    textMod.setPosition(SCR_LARG/2 - 300, SCR_HAUT / 3);
    pressButtonForTitle.setCharacterSize(50);
    pressButtonForTitle.setString("<Press spacebar to go back to Main Menu>");
    pressButtonForTitle.setPosition(SCR_LARG / 2 - 570, SCR_HAUT / 3 + 200);

    while (stopGame)
    {
        while (gameWindow.pollEvent(ev))
        {
            switch (ev.type)
            {
            case Event::Closed:
                gameWindow.close();
            case Event::KeyPressed:
                if (ev.key.code == Keyboard::Space)
                {
                    stopGame = false;
                }
            default:
                break;
            }
        }
        gameWindow.clear(Color::Black);
        gameWindow.draw(gameOver);
        gameWindow.draw(textMod);
        gameWindow.draw(pressButtonForTitle);
        gameWindow.display();
    }
    return stopGame;
}



void multiGame(RenderWindow &gameWindow, Text textMod)
{
	//                                     CONNECTION TO SERVER
/*-------------------------------------------------------------------------------------------*/

string serverName;
string clientName;
IpAddress ip;
string mtrxSend;

ICParams ic = {&serverName, &clientName,  &mtrxSend, &ip};

Thread* initConnection = 0;  // We create a thread so that the window doesn't freeze

initConnection = new Thread(&initClient, ic);
initConnection->launch();


//                                      PRE GAME SETTINGS
/*-------------------------------------------------------------------------------------------*/

    //LOAD FILES

        std::vector<Sprite> boxWall;        /*                       */
        std::vector<Sprite> boxFence;       /*      MAP VECTORS      */
        std::vector<Sprite> boxBush;        /*                       */
        std::vector<Sprite> boxTree;        /*                       */

        //TEXTURES FOR MAP STUFF

            Texture wallTexture;
            wallTexture.loadFromFile("Sprites/wall.png");
            Texture fenceTexture;
            fenceTexture.loadFromFile("Sprites/fence.png");
            Texture bushTexture;
            bushTexture.loadFromFile("Sprites/bush.png");
            Texture treeTexture;
            treeTexture.loadFromFile("Sprites/tree.png");
            Texture aimTexture;
            Background bg;

        //AIM SPRITE SETUP

            aimTexture.loadFromFile("Sprites/aim.png");
            Sprite aim;
            aim.setTexture(aimTexture);
            aim.setOrigin(10,10);

        //SOUNDS LOAD

            Sound shoot;
            Music music;
            music.openFromFile("Audio/gameSound.ogg");  //Start music over and over
            music.play();
            music.setLoop(true);
            SoundBuffer gameOverBuf;                    //Play when the player dies
            gameOverBuf.loadFromFile("Audio/gameOver.wav");
            Sound gameOverSound;
            gameOverSound.setBuffer(gameOverBuf);

    //WINDOW SECURITY

        gameWindow.setFramerateLimit(60);
        gameWindow.setMouseCursorVisible(false);

    //CREATE A NEW MAP

        if(initConnection)
        {
            initConnection->wait();
            delete initConnection;
        }



        size_t idx = 0;
        short mtrxObstcl[NB_COLUMN][NB_ROW];  //Used for map display and detections of walls

        for (size_t i = 0; i < NB_COLUMN; i++)
        {
            for (size_t j = 0; j < NB_ROW; j++)
            {

                mtrxObstcl[i][j] = mtrxSend[idx] - 48;
                idx++;

                cout << mtrxObstcl[i][j];
            }
        }

        bg.displayBg(gameWindow);                                           //we load the background, the value 1 say if we have to load the texture or not (1if we never load the texture and 0 if we already load the texture)
        LoadMap(mtrxObstcl,gameWindow,boxWall,boxFence,boxBush,boxTree);    //we load the map with the table created above into sprites
        DisplayMap(gameWindow, boxWall, boxFence, boxBush, boxTree, wallTexture, fenceTexture, bushTexture, treeTexture);

/*-------------------------------------------------------------------------------------------*/

   //VARIABLES

        Event ev;
        Personnage player1;
        Personnage player2;
        Projectile shotP1;
        Projectile shotP2;

        if (clientName == "CLIENT1")
        {
            player1.changeTexture("Sprites/Joueur1.png");
            player2.changeTexture("Sprites/Joueur2.png");

            shotP1.setTexture("Sprites/BalleJ1.png");
            shotP2.setTexture("Sprites/BalleJ2.png");
        } else {
            player1.changeTexture("Sprites/Joueur2.png");
            player2.changeTexture("Sprites/Joueur1.png");

            shotP1.setTexture("Sprites/BalleJ2.png");
            shotP2.setTexture("Sprites/BalleJ1.png");
        }

        player1.changePosition(SCR_LARG/2, SCR_HAUT/2);
        player2.changePosition(SCR_LARG/2, SCR_HAUT/2);

        Ghost ghost;

        int ghostSpawn = 750, newGhost = 900, increaseGhostSpeed = 0, invinsibleTime = 50, gameIsWin = true;

        //VECTORS

            std::vector<Projectile> projectilesP1;
            std::vector<Projectile> projectilesP2;

        // VARS FOR THE NETWORK
        string dataSend;
        string dataReceive;
        string receivedDuplicate;
        string sendedDuplicate;
        string shooting;
        int mouseX, mouseY;

        // CLIENT THREAD
        Thread* clientThread = 0;

        ClientParams cp = {&dataSend, &dataReceive, &receivedDuplicate, &sendedDuplicate, &ip, &player2, &shooting, &mouseX, &mouseY, &gameIsWin};

        clientThread = new Thread(&Client, cp);
        clientThread->launch();

   //GAME LOOP

        while (gameIsWin)
        {

        //EVENT POLLING

            while (gameWindow.pollEvent(ev))
            {
                switch(ev.type)
                {
                case Event::Closed:
                    gameWindow.close();
                    gameIsWin = false;
                    break;
                default:
                    break;
                }
            }

        //UPDATES

            //Update aim

                aim.setPosition(Mouse::getPosition(gameWindow).x, Mouse::getPosition(gameWindow).y);

            //Update aim direction

                shotP1.dirNormUpdate(player1,ev,gameWindow);
                shotP2.dirNormUpdate(player2,ev,gameWindow);

            //Update player
                player1.mvtPerso(ev, mtrxObstcl);
                player1.SuisLaMouse(gameWindow);

                if (player1.reload < TPS_RELOAD)  //Reload time
                {
                    player1.reload++;
                }

                if (player2.reload < TPS_RELOAD)  //Reload time
                {
                    player2.reload++;
                }

            //Create a new projectile

                if (Mouse::isButtonPressed(Mouse::Left) && player1.reload >= TPS_RELOAD) //Needs to have reloaded before shooting
                {
                    player1.reload = 0;
                    shotP1.setPosition(Vector2f(player1.persoSprite.getPosition()));
                    shotP1.currVelocity = shotP1.aimDirNorm * shotP1.speed;
                    projectilesP1.push_back(Projectile(shotP1));
                    player1.shoot.play();
                }


                if (shooting == "shoot" && player2.reload >= TPS_RELOAD) //Needs to have reloaded before shooting
                {
                    Vector2f playerCenter = Vector2f(player2.persoSprite.getPosition());
                    shotP2.setPosition(playerCenter);
                    Vector2f mousePosWindow = Vector2f(mouseX, mouseY);
                    Vector2f aimDir = mousePosWindow - playerCenter;
                    shotP2.aimDirNorm = aimDir / (float) sqrt(pow(aimDir.x, 2 )+ pow(aimDir.y , 2));

                    player2.reload = 0;
                    shotP2.setPosition(Vector2f(player2.persoSprite.getPosition()));
                    shotP2.currVelocity = shotP2.aimDirNorm * shotP2.speed;
                    projectilesP2.push_back(Projectile(shotP2));
                    player2.shoot.play();
                }
            //Update projectile

                for (size_t i = 0; i < projectilesP1.size(); i++)
                {

                    //Collision detections

                    //Vertical Side Borders
                    if (projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 <= BORDER_X || projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 >= SCR_LARG - BORDER_X) // IF HIT LEFT OR RIGHT
                    {
                        projectilesP1[i].currVelocity.x = -projectilesP1[i].currVelocity.x;
                        projectilesP1[i].nbCol++;
                    }
                    //Vertical Side Blocks
                    if (mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectilesP1[i].currVelocity.x = -projectilesP1[i].currVelocity.x;
                        projectilesP1[i].nbCol++;
                        projectilesP1[i].projectileSprite.move(projectilesP1[i].currVelocity);
                    }
                    //Horizontal Side Border
                    if (projectilesP1[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 <= BORD_LARG || projectilesP1[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 >= SCR_HAUT - BORD_LARG) // IF HIT UP OR DOWN
                    {
                        projectilesP1[i].currVelocity.y = -projectilesP1[i].currVelocity.y;
                        projectilesP1[i].nbCol++;
                    }
                    //Horizontal Side Blocks
                    if (mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP1[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP1[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectilesP1[i].currVelocity.y = -projectilesP1[i].currVelocity.y;
                        projectilesP1[i].nbCol++;
                        projectilesP1[i].projectileSprite.move(projectilesP1[i].currVelocity);
                    }
                    //If projectile collided twice, then destroy object
                        if (projectilesP1[i].nbCol >= 3)
                            projectilesP1.erase(projectilesP1.begin() + i);
                    //Else test if projectile hit a ghost
                        else
                        {
                            if (projectilesP1[i].projectileSprite.getGlobalBounds().intersects(player2.persoSprite.getGlobalBounds())) //If projectile hit a ghost
                            {
                                projectilesP1.erase(projectilesP1.begin() + i); //Destroy projectile that hit player
                                break;
                            }
                        }
                        projectilesP1[i].projectileSprite.move(projectilesP1[i].currVelocity); //Update projectile Position
                }

                for (size_t i = 0; i < projectilesP2.size(); i++)
                {

                    //Collision detections

                    //Vertical Side Borders
                    if (projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 <= BORDER_X || projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 >= SCR_LARG - BORDER_X) // IF HIT LEFT OR RIGHT
                    {
                        projectilesP2[i].currVelocity.x = -projectilesP2[i].currVelocity.x;
                        projectilesP2[i].nbCol++;
                    }
                    //Vertical Side Blocks
                    if (mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + 5 - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - 5 + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectilesP2[i].currVelocity.x = -projectilesP2[i].currVelocity.x;
                        projectilesP2[i].nbCol++;
                        projectilesP2[i].projectileSprite.move(projectilesP2[i].currVelocity);
                    }
                    //Horizontal Side Border
                    if (projectilesP2[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 <= BORD_LARG || projectilesP2[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 >= SCR_HAUT - BORD_LARG) // IF HIT UP OR DOWN
                    {
                        projectilesP2[i].currVelocity.y = -projectilesP2[i].currVelocity.y;
                        projectilesP2[i].nbCol++;
                    }
                    //Horizontal Side Blocks
                    if (mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 1 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x - 5 + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y - TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + 5 - TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2 || mtrxObstcl[(short)((projectilesP2[i].projectileSprite.getPosition().x + TAIL_PROJ / 2 - BORDER_X) / 80)][(short)((projectilesP2[i].projectileSprite.getPosition().y + TAIL_PROJ / 2 - BORDER_Y) / 80)] == 2)
                    {
                        projectilesP2[i].currVelocity.y = -projectilesP2[i].currVelocity.y;
                        projectilesP2[i].nbCol++;
                        projectilesP2[i].projectileSprite.move(projectilesP2[i].currVelocity);
                    }
                    //If projectile collided twice, then destroy object
                        if (projectilesP2[i].nbCol >= 3)
                            projectilesP2.erase(projectilesP2.begin() + i);
                    //Else test if projectile hit a ghost
                        else
                        {
                            if (projectilesP2[i].projectileSprite.getGlobalBounds().intersects(player1.persoSprite.getGlobalBounds())) //If projectile hit a player
                            {
                                projectilesP2.erase(projectilesP2.begin() + i); //Destroy projectile that hit player
                                break;
                            }
                        }
                        projectilesP2[i].projectileSprite.move(projectilesP2[i].currVelocity); //Update projectile Position
                }

        //RENDER

            gameWindow.clear(Color::Black); //Clear window

            bg.displayBg(gameWindow); //we display the background
            player2.afficherPerso(gameWindow); //Display player under the sprites of the map so that the bushes and dead trees are on top of the player
            player1.afficherPerso(gameWindow);
            DisplayMap(gameWindow,boxWall,boxFence,boxBush,boxTree,wallTexture,fenceTexture,bushTexture,treeTexture); //Display all the sprites load with the function LoadMap
            //Draw all projectiles

                for(size_t i = 0; i < projectilesP1.size(); i++)
                {
                    projectilesP1[i].drawProj(gameWindow);
                }

                for(size_t i = 0; i < projectilesP2.size(); i++)
                {
                    projectilesP2[i].drawProj(gameWindow);
                }

            //Show score and life

                player1.showScore(gameWindow, textMod);
                player1.showLife(gameWindow);

            //Draw aim on top of everything

                gameWindow.draw(aim);

            //Display window

                gameWindow.display();

        //CHECK IF PLAYER LOST

            if (player1.nbDeVie == 0)
                {
                    sleep(seconds(1)); //Break for 1 seconds
                    music.stop();
                    gameOverSound.play();
                    gameIsWin = gameOverScreen(gameWindow, textMod, player1.score, gameIsWin);   //Game Over Screen
                }

                // SEND THE DATA
                ostringstream tmp;

                tmp << clientName << ":PLAYER:" << player1.pos.x << "x" << player1.pos.y << "y" << player1.angle << "r";  // For the player
                tmp << ":PROJ:";  // For the projectile
                if (Mouse::isButtonPressed(Mouse::Left))
                {
                    tmp << "shoot:";
                    tmp << Mouse::getPosition(gameWindow).x << "x" << Mouse::getPosition(gameWindow).y << "y";
                }
                else
                    tmp << ":";

                dataSend = tmp.str();
        }

    //END OF APPLICATION

}

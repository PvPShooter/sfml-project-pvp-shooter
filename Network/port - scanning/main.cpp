#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <sstream>

using namespace sf;
using namespace std;

int main()
{
    UdpSocket socket;
    unsigned short port = 53000;
    string address = "10.103.252.3";

    socket.bind(port);

    ostringstream ipPattern;
    ostringstream tmp;

    tmp << IpAddress::getLocalAddress();

    ipPattern << tmp.str().erase(tmp.str().rfind(".")+1, 5);
    tmp.str("");
    tmp.clear();

    tmp << ipPattern.str();

    int i;

    for (i = 0; i < 254; i++)
    {
        ipPattern << i;
        cout << ipPattern.str() << endl;
        ipPattern.str("");
        ipPattern.clear();
        ipPattern << tmp.str();
    }

    return EXIT_SUCCESS;
}

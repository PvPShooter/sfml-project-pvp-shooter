#include "IO.hpp"

using namespace sf;
using namespace std;

Mutex globalMutex;  // The mutex

SocketSelector selector;

void IOFunc(UdpSocket* socket, IpAddress ip, unsigned short localPort, unsigned short distPort, string* dataSend, string* dataReceive, string* oldData, bool* quit, string IO)  // The I/O function
{
    if ( IO == ONLY_SEND )
    {
        Packet packetSend;  // Define the packet
        globalMutex.lock();  // We use the mutex so that the data is not mixed with other things
        packetSend << (*dataSend);  // The packet gather the data to send
        globalMutex.unlock();
        (*socket).send(packetSend, ip, distPort);  // And... Sending !
    }

    if ( IO == ONLY_RECEIVE )
    {
        selector.add(*socket);

        Packet packetReceive;

        if (selector.wait(milliseconds(1)))
        {
            (*socket).receive(packetReceive, ip, localPort);

            if ((packetReceive >> (*dataReceive)) && (*dataReceive) != (*oldData) && !(*dataReceive).empty())
            {
                (*oldData) = (*dataReceive);
            }
        }

        selector.clear();
    }

}

void getInput(GIParams gi)
{
    while (!(*gi.quit))
    {
        cin >> (*gi.dataSend);
    }
}

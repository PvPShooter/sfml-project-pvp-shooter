#ifndef DEF_IO
#define DEF_IO

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define ONLY_SEND (string) "send"
#define ONLY_RECEIVE (string) "receive"

using namespace sf;
using namespace std;

struct GIParams
{
    string* dataSend;
    bool* quit;
};

void IOFunc(UdpSocket* socket, IpAddress ip, unsigned short localPort, unsigned short distPort, string* dataSend, string* dataReceive, string* oldData, bool* quit, string IO);
void getInput(GIParams gi);

#endif // DEF_IO

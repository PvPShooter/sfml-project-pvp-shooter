#include "server.hpp"
#include "client.hpp"
#include "IO.hpp"

using namespace sf;
using namespace std;

int main()
{
    string dataSend;  // The var for the data sending
    string dataReceive;  // The var for the data we receive

    char who;  // For server or client
    bool quit = false;  // The boolean for quiting

    unsigned short localPort;  // For the local port of the server
    unsigned short distPort;  // For the distant port of the server

    Thread* serverThread = 0;  // The thread for the server

    cout << "Do you want to be a server (s) or a client (c) ? ";
    cin  >> who;

    if ( who == 's') // If we choose server
    {
        string serverName;
        cout << "Choose your server name: ";
        cin >> serverName;

        localPort = 53000;
        distPort = 53001;

        SParams ps = {localPort,distPort, &dataSend, &dataReceive, &quit, serverName}; // *dataSend, *dataReceive, *quit

        serverThread = new Thread(&Server, ps);
        serverThread->launch();  // Launch the server thread
    }
    else  // If we choose client
        Client(&dataSend, &dataReceive, &quit);  // Launch the client function

    if(serverThread)
    {
        serverThread->wait();
        delete serverThread;
    }

    return 0;
}

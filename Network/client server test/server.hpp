#ifndef DEF_SERVER
#define DEF_SERVER

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <string.h>

using namespace sf;
using namespace std;

struct SParams
{
    unsigned short localPort;
    unsigned short distPort;
    string* dataSend;
    string* dataReceive;
    bool* quit;
    string serverName;
};

void Server(SParams ps);

#endif // DEF_SERVER

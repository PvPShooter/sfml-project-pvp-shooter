#include "client.hpp"
#include "IO.hpp"

using namespace sf;
using namespace std;

static string oldData; /* The var for the old data received, in case of duplication
                        so that the data is not interpreted as the actions are already done*/

void Client(string* dataSend, string* dataReceive, bool* quit)
{
    IpAddress ip = "localhost";  // For the ip address
    unsigned short localPort, distPort;  // For the port
    string serverName;  // The server name
    string clientName;  // The client number

    // THE BEGINNING CONNECTION
    ostringstream ipPattern;
    ostringstream PosYCoord;

    PosYCoord << IpAddress::getLocalAddress();

    ipPattern << PosYCoord.str().erase(PosYCoord.str().rfind(".")+1, 5);
    PosYCoord.str("");
    PosYCoord.clear();

    PosYCoord << ipPattern.str();

    int i;

    for (i = 0; i < 254; i++)
    {
        TcpSocket initSock;
        if (initSock.connect(ip, 53000, milliseconds(10)) != Socket::Done)
        {
            ipPattern << i;
            ip = ipPattern.str();
            cout << ip << endl;
            ipPattern.str("");
            ipPattern.clear();
            ipPattern << PosYCoord.str();
        }
        else
        {
            Packet packet;
            initSock.receive(packet);

            packet >> serverName >> clientName;
            initSock.disconnect();
            break;
        }
    }

    cout << "Server name: " << serverName << " Client name: " << clientName << " IP: " << ip << endl;
    /////////////////////////////////////////////////////////////////////////////



    UdpSocket clientSocket;  // The socket
    localPort = 53001;  // Local port
    distPort = 53000;  // Dist port
    clientSocket.bind(localPort);  // We bind on the socket

    string receivedDuplicate;  // If the data received is the same twice
    string sendedDuplicate;  // If the data sended is the same twice

    Thread* GIThread = 0;

    GIParams gi = {dataSend, quit};  // *dataSend, *quit
    GIThread = new Thread(&getInput, gi);
    GIThread->launch();

    while (!(*quit))
    {
        IOFunc(&clientSocket, ip, localPort, distPort, dataSend, dataReceive, &oldData, quit, ONLY_RECEIVE);

        if ( (*dataSend) != sendedDuplicate && !(*dataSend).empty() )
        {
            IOFunc(&clientSocket, ip, localPort, distPort, dataSend, dataReceive, &oldData, quit, ONLY_SEND);
            sendedDuplicate = (*dataSend);
        }

        if (( *dataReceive) != receivedDuplicate && !(*dataReceive).empty() )
        {
            size_t foundComma = (*dataReceive).find(":");  // Find the ":"

            (*dataReceive).erase(0, foundComma+1);  // We delete the "CLIENTX:" str

            foundComma = (*dataReceive).find(":");
            string method = (*dataReceive).substr(0, foundComma);


            if ( method == "MAP" )
            {

            }

            else if ( method == "PLAYER" )
            {
                (*dataReceive).erase(0, foundComma+1);
                size_t foundPosX = (*dataReceive).find("x");

                stringstream PosXCoord( (*dataReceive).substr(0, foundPosX) );
                int PosXReceive;
                PosXCoord >> PosXReceive;

                (*dataReceive).erase(0, foundPosX+1);
                size_t foundPosY = (*dataReceive).find("y");

                stringstream PosYCoord( (*dataReceive).substr(0, foundPosY) );
                int PosYReceive;
                PosYCoord >> PosYReceive;

                cout << "The x: " << PosXReceive << "The y: " << PosYReceive << endl;
            }

            else if ( method == "PROJECTILE" )
            {

            }

            receivedDuplicate = (*dataReceive);  // Not to duplicate
        }

    }

    if ( GIThread )
    {
        GIThread->wait();
        delete GIThread;
    }

}

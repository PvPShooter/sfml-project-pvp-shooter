#ifndef DEF_CLIENT
#define DEF_CLIENT

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <string.h>

using namespace sf;
using namespace std;

void Client(string* dataSend, string* dataReceive, bool* quit);

#endif // DEF_CLIENT

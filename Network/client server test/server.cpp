#include "server.hpp"
#include "IO.hpp"

using namespace sf;
using namespace std;

static string oldData; /* The var for the old data received, in case of duplication
                        so that the data is not interpreted as the actions are already done*/

void Server(SParams ps) // *dataSend, *dataReceive, *quit;
{
    /*ostringstream ipPattern;
    ostringstream tmp;

    tmp << IpAddress::getLocalAddress();

    ipPattern << tmp.str().erase(tmp.str().rfind(".")+1, 5);
    tmp.str("");
    tmp.clear();

    tmp << ipPattern.str();

    ostringstream dataConcat;
    dataConcat << "GYBECONNECT:" << IpAddress::getLocalAddress();
    *ps.dataSend = dataConcat.str();

    int i;

    TcpListener listener;
    listener.listen(ps.localPort);
    while (1)
    {
        for (i = 0; i < 254; i++)
        {
            if (listener.listen(ps.localPort))
            {
                ipPattern << i;
                ip = ipPattern.str();
                IOFunc(&serverSocket, "192.168.1.100", ps.localPort, ps.distPort, ps.dataSend, ps.dataReceive, &oldData, ps.quit, ONLY_SEND);
                ipPattern.str("");
                ipPattern.clear();
                ipPattern << tmp.str();
            }
            else
            {
                break;
            }
        }
    }*/

    IpAddress ip1, ip2;
    //  THE CONNECTION

    TcpListener initListen;
    initListen.listen(53000);
    Packet packetClient1;
    Packet packetClient2;

    TcpSocket initSock;
    if  ( initListen.accept(initSock) == Socket::Done )
        ip1 = initSock.getRemoteAddress();

    packetClient1 << ps.serverName << "CLIENT1";
    initSock.send(packetClient1);

    initSock.disconnect();

    if  ( initListen.accept(initSock) == Socket::Done )
        ip2 = initSock.getRemoteAddress();

    packetClient2 << ps.serverName << "CLIENT2";
    initSock.send(packetClient2);

    initSock.disconnect();
    initListen.close();


    cout << "client1: " << ip1 << ", client2: " << ip2 << endl;
    ///////////////////////////////////////////////////////////////////////


    UdpSocket serverSocket; // The socket
    serverSocket.bind(ps.localPort);
    string sendedDuplicate;


    while (!(*ps.quit))
    {
        (*ps.dataSend) = (*ps.dataReceive);  // We wait to send what we got

        if ( (*ps.dataReceive).substr(0, (*ps.dataReceive).find(":")) == "CLIENT1" && (*ps.dataSend) != sendedDuplicate )  // If beginning is CLIENT1 -> CLIENT2
        {
            IOFunc(&serverSocket, ip2, ps.localPort, ps.distPort, ps.dataSend, ps.dataReceive, &oldData, ps.quit, ONLY_SEND);
            sendedDuplicate = (*ps.dataSend);
        }

        if ( (*ps.dataReceive).substr(0, (*ps.dataReceive).find(":")) == "CLIENT2" && (*ps.dataSend) != sendedDuplicate )  // If beginning is CLIENT2 -> CLIENT1
        {
            IOFunc(&serverSocket, ip1, ps.localPort, ps.distPort, ps.dataSend, ps.dataReceive, &oldData, ps.quit, ONLY_SEND);
            sendedDuplicate = (*ps.dataSend);
        }

        (*ps.dataSend) = "";

        IOFunc(&serverSocket, ip1, ps.localPort, ps.distPort, ps.dataSend, ps.dataReceive, &oldData, ps.quit, ONLY_RECEIVE);
        IOFunc(&serverSocket, ip2, ps.localPort, ps.distPort, ps.dataSend, ps.dataReceive, &oldData, ps.quit, ONLY_RECEIVE);
    }

    return;
}

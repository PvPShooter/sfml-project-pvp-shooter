#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "constantes.hpp"
#include "menu.hpp"
#include "modeJeu.hpp"

using namespace sf;

int main(int argc, char* argv[])
{

    //Load Files

        Font police;

        if(!police.loadFromFile(POLICE))
            printf("Error loading %s", POLICE);
        Text textMod;
        textMod.setFont(police);
        textMod.setColor(Color::White);
        textMod.setCharacterSize(400);

        Sprite spr;

        srand(time(NULL));
    //Variables

    int modeJeu = 0;


    //Window stuff

    RenderWindow gameScreen(VideoMode(SCR_LARG, SCR_HAUT), "GYBE", Style::Titlebar | Style::Close);
    gameScreen.clear(Color(250,230,230));

    while (gameScreen.isOpen())
    {
    modeJeu = titleScreen(gameScreen, textMod); // Show Title screen to select game mode
    if (modeJeu == 1)
        survivalGame(gameScreen, textMod);
    else if (modeJeu == 2)
        multiGame(gameScreen);
    }


    return EXIT_SUCCESS;
}

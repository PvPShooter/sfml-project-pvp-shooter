#ifndef DEF_JEU
#define DEF_JEU
#include <SFML/Graphics.hpp>

using namespace sf;

void survivalGame(RenderWindow &gameWindow, Text textMod);
void multiGame(RenderWindow &gameWindow);
int gameOverScreen(RenderWindow &gameWindow, Text textMod, int score, int stopGame);

#endif // DEF_JEU

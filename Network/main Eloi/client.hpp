#ifndef DEF_CLIENT
#define DEF_CLIENT

#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.hpp"
#include "projectile.hpp"

using namespace sf;
using namespace std;

struct ICParams
{
    string* serverName;
    string* clientName;
    IpAddress* ip;
};

struct ClientParams
{
    string* dataSend;
    string* dataReceive;
    string* receivedDuplicate;  // If the data received is the same twice
    string* sendedDuplicate;  // If the data sended is the same twice
    IpAddress* ip;
    Personnage* player;
    string* shooting;
    int* mouseX;
    int* mouseY;
    int* gameIsWin;
};

void initClient(ICParams ic);
void Client(ClientParams cp);

#endif // DEF_CLIENT

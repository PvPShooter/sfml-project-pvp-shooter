#ifndef DEF_PROJECTILE
#define DEF_PROJECTILE
#include "constantes.hpp"
#include "personnage.hpp"

typedef struct

    {
        float x;
        float y;
    } Tir;

class Projectile
{
public:

    Projectile();

    Vector2f pos;
    Vector2f currVelocity;
    Vector2f aimDirNorm;
    float speed;
    int angle;
    int nbCol;

    Sprite projectileSprite;
    Texture projectileTexture;

    float collide();
    void dirNormUpdate(Personnage& player, Event& ev, RenderWindow& gameWindow);
    void damage(int damage);
    void setTexture(char link[]);
    void projectileMvmt(int angle, Vector2f pos);
    void drawProj(RenderWindow &gameWindow);
    void setPosition(Vector2f pos);
};
#endif // DEF_PROJECTILE



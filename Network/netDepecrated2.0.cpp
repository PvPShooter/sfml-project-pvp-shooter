#include <iostream>
#include <sstream>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdlib.h>
#include <string.h>

using namespace sf;
using namespace std;

const unsigned short PORT = 5000;
const IpAddress IPADDRESS("10.103.252.2"); //change to suit your needs

string msgSend;

TcpSocket client1;
TcpSocket client2;
Mutex globalMutex;
bool quit = false;

void DoStuff(void)
{
    static string oldMsg1;
    static string oldMsg2;

    RenderWindow app(VideoMode(800, 600), "TEST");
    app.setMouseCursorVisible(false);

    CircleShape serverCircle(50);
    serverCircle.setOrigin(50, 50);
    CircleShape clientCircle(50);
    clientCircle.setOrigin(50, 50);

    int xReceive = 0, yReceive = 0;
    int xSend = 0, ySend = 0;
    int rReceive = 255, gReceive = 255, bReceive = 255;
    int rSend = 255, gSend = 255, bSend = 255;

    Event event;
    ostringstream test;

    while(!quit)
    {
        Packet packetSend;
        globalMutex.lock();
        packetSend << msgSend;
        globalMutex.unlock();
        client1.send(packetSend);
        client2.send(packetSend);

        string msg1;
        string msg2;
        Packet packetReceive1;
        Packet packetReceive2;

        client1.receive(packetReceive1);
        if ((packetReceive1 >> msg1) && msg1 != oldMsg1 && !msg1.empty())
        {
            cout << msg1 << endl;
            oldMsg1 = msg1;

            // Position
            size_t foundX = msg1.find("x");
            stringstream xCoords(msg1.substr(0, foundX));
            xCoords >> xReceive;
            msg1.erase(0, foundX + 1);  // We erase the value and the "x"

            size_t foundY = msg1.find("y");
            stringstream yCoords(msg1.substr(0, foundY));
            yCoords >> yReceive;
            msg1.erase(0, foundY + 1);  // We erase the value and the "y"

            // Color
            size_t foundR = msg1.find("r");
            stringstream rColor(msg1.substr(0, foundR));
            rColor >> rReceive;
            msg1.erase(0, foundR + 1);  // We erase the value and the "x"

            size_t foundG = msg1.find("g");
            stringstream gColor(msg1.substr(0, foundG));
            gColor >> gReceive;
            msg1.erase(0, foundG + 1);  // We erase the value and the "x"

            size_t foundB = msg1.find("b");
            stringstream bColor(msg1.substr(0,foundB));
            bColor >> bReceive;
            msg1.erase(0, foundB + 1);  // We erase the value and the "x"

            serverCircle.setPosition(xReceive, yReceive);
            serverCircle.setFillColor(Color(rReceive, gReceive, bReceive));
        }

        client2.receive(packetReceive2);
        if ((packetReceive2 >> msg2) && msg2 != oldMsg2 && !msg2.empty())
        {
            cout << msg2 << endl;
            oldMsg2 = msg2;

            // Position
            size_t foundX = msg2.find("x");
            stringstream xCoords(msg2.substr(0, foundX));
            xCoords >> xReceive;
            msg2.erase(0, foundX + 1);  // We erase the value and the "x"

            size_t foundY = msg2.find("y");
            stringstream yCoords(msg2.substr(0, foundY));
            yCoords >> yReceive;
            msg2.erase(0, foundY + 1);  // We erase the value and the "y"

            // Color
            size_t foundR = msg2.find("r");
            stringstream rColor(msg2.substr(0, foundR));
            rColor >> rReceive;
            msg2.erase(0, foundR + 1);  // We erase the value and the "x"

            size_t foundG = msg2.find("g");
            stringstream gColor(msg2.substr(0, foundG));
            gColor >> gReceive;
            msg2.erase(0, foundG + 1);  // We erase the value and the "x"

            size_t foundB = msg2.find("b");
            stringstream bColor(msg2.substr(0,foundB));
            bColor >> bReceive;
            msg2.erase(0, foundB + 1);  // We erase the value and the "x"

            serverCircle.setPosition(xReceive, yReceive);
            serverCircle.setFillColor(Color(rReceive, gReceive, bReceive));
        }



        // Process events
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();

            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::Space)
                {
                    rSend = rand()%254+1;
                    gSend = rand()%254+1;
                    bSend = rand()%254+1;
                }

                if (event.key.code == Keyboard::Escape)
                {
                    app.close();
                    quit = true;
                }
            }

            if ( event.type == Event::MouseMoved)
            {
                xSend = event.mouseMove.x;
                ySend = event.mouseMove.y;
            }

            test << xSend;
            test << "x";
            test << ySend;
            test << "y";
            test << rSend;
            test << "r";
            test << gSend;
            test << "g";
            test << bSend;
            test << "b";
            msgSend = test.str();
            test.str("");
            test.clear();
        }
        clientCircle.setPosition(xSend, ySend);
        clientCircle.setFillColor(Color(rSend, gSend, bSend));
        app.clear();
        app.draw(serverCircle);
        app.draw(clientCircle);
        app.display();
    }
}

void Server(void)
{
    int client = 1;
    while(!quit)
    {
        if (client == 1)
        {
            TcpListener listener1;
            listener1.listen(PORT);
            listener1.accept(client1);
            cout << "Client 1: " << client1.getRemoteAddress() << endl;
            client++;
        }

        if (client == 2)
        {
            TcpListener listener2;
            listener2.listen(PORT);
            listener2.accept(client2);
            cout << "Client 2: " << client2.getRemoteAddress() << endl;
        }
    }
}

bool Client(void)
{
    if(client1.connect(IPADDRESS, PORT) == Socket::Done)
    {
        cout << "Connected\n";
        return true;
    }
    return false;
}

int main(int argc, char* argv[])
{
    srand(time(NULL));

    Thread* dostuffThread = 0; // The thread for the function DoStuff
    Thread* serverThread = 0; // The thread for the function Server

    char who;
    cout << "Do you want to be a server (s) or a client (c) ? ";
    cin  >> who;

    if (who == 's')
    {
        serverThread = new Thread(&Server);
        serverThread->launch();
    }
    else
        Client();

    dostuffThread = new Thread(&DoStuff);
    dostuffThread->launch();

    if(dostuffThread)
    {
        cout << "DEBUG\n";
        dostuffThread->wait();
        delete dostuffThread;
    }

    if(serverThread)
    {
        serverThread->wait();
        delete serverThread;
    }

    return 0;
}

#include <iostream>
#include <sstream>
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdlib.h>
#include <string.h>

using namespace sf;
using namespace std;

const unsigned short PORT = 5000;
const IpAddress IPADDRESS("10.103.252.3"); //change to suit your needs

string msgSend;

TcpSocket socket;
Mutex globalMutex;
bool quit = false;

void DoStuff(void)
{
    static string oldMsg;

    RenderWindow app(VideoMode(800, 600), "TEST");
    CircleShape serverCircle(50);
    serverCircle.setOrigin(50, 50);
    CircleShape clientCircle(50);
    clientCircle.setOrigin(50, 50);

    int xReceive = 0, yReceive = 0;
    int xSend = 0, ySend = 0;
    int rReceive = 255, gReceive = 255, bReceive = 255;
    int rSend = 255, gSend = 255, bSend = 255;

    Event event;
    ostringstream test;

    while(!quit)
    {
        Packet packetSend;
        globalMutex.lock();
        packetSend << msgSend;
        globalMutex.unlock();
        socket.send(packetSend);

        string msg;
        Packet packetReceive;

        socket.receive(packetReceive);
        if ((packetReceive >> msg) && msg != oldMsg && !msg.empty())
        {
            cout << msg << endl;
            oldMsg = msg;

            // Position
            size_t foundX = msg.find("x");
            stringstream xCoords(msg.substr(0, foundX));
            xCoords >> xReceive;
            msg.erase(0, foundX + 1);  // We erase the value and the "x"

            size_t foundY = msg.find("y");
            stringstream yCoords(msg.substr(0, foundY));
            yCoords >> yReceive;
            msg.erase(0, foundY + 1);  // We erase the value and the "y"

            // Color
            size_t foundR = msg.find("r");
            stringstream rColor(msg.substr(0, foundR));
            rColor >> rReceive;
            msg.erase(0, foundR + 1);  // We erase the value and the "x"

            size_t foundG = msg.find("g");
            stringstream gColor(msg.substr(0, foundG));
            gColor >> gReceive;
            msg.erase(0, foundG + 1);  // We erase the value and the "x"

            size_t foundB = msg.find("b");
            stringstream bColor(msg.substr(0,foundB));
            bColor >> bReceive;
            msg.erase(0, foundB + 1);  // We erase the value and the "x"

            serverCircle.setPosition(xReceive, yReceive);
            serverCircle.setFillColor(Color(rReceive, gReceive, bReceive));
        }



        // Process events
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();

            if (event.type == Event::KeyPressed)
            {
                if (event.key.code == Keyboard::Space)
                {
                    rSend = rand()%254+1;
                    gSend = rand()%254+1;
                    bSend = rand()%254+1;
                }
            }

            if ( event.type == Event::MouseMoved)
            {
                xSend = event.mouseMove.x;
                ySend = event.mouseMove.y;
            }

            test << xSend;
            test << "x";
            test << ySend;
            test << "y";
            test << rSend;
            test << "r";
            test << gSend;
            test << "g";
            test << bSend;
            test << "b";
            msgSend = test.str();
            test.str("");
            test.clear();
        }
        clientCircle.setPosition(xSend, ySend);
        clientCircle.setFillColor(Color(rSend, gSend, bSend));
        app.clear();
        app.draw(serverCircle);
        app.draw(clientCircle);
        app.display();
    }
}

void Server(void)
{
    TcpListener listener;
    listener.listen(PORT);
    listener.accept(socket);
    cout << "New client connected: " << socket.getRemoteAddress() << endl;
}

bool Client(void)
{
    if(socket.connect(IPADDRESS, PORT) == Socket::Done)
    {
        cout << "Connected\n";
        return true;
    }
    return false;
}

void GetInput(void)
{
    string s;
    //cout << "\nEnter \"exit\" to quit or message to send: ";
    getline(cin,s);
    if(s == "exit")
        quit = true;
    globalMutex.lock();
    msgSend = s;
    globalMutex.unlock();
}

int main(int argc, char* argv[])
{
    srand(time(NULL));

    Thread* thread = 0; // The thread for the function DoStuff
    Thread* graphThread = 0; // The thread for the graphic function

    char who;
    cout << "Do you want to be a server (s) or a client (c) ? ";
    cin  >> who;

    if (who == 's')
    {
        //graphThread = new Thread(&graph, circle);
        //graphThread->launch();
        Server();
    }
    else
        Client();

    thread = new Thread(&DoStuff);
    thread->launch();

    while (!quit)
    {
        GetInput();
    }

    if(thread)
    {
        thread->wait();
        delete thread;
    }

    if(graphThread)
    {
        graphThread->wait();
        delete graphThread;
    }

    return 0;
}
